import React from 'react';
import Link from "next/link";
import Image from "next/image";

import Logo from 'public/main/images/logo.svg';
import classes from './footer.module.scss';



const Footer = () => {
  return (
    <>
      <div className={classes.roadmapWrap}>
        <div className={classes.roadmapInner}>
          <div className={classes.roadMapWrap}>
            <img src="images/roadmap-bg.png" alt="Roadmap icon" />
          </div>
          <div className='container'>
            <div className={classes.roadMapHeader}>
              <h2>Roadmap</h2>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
            </div>
            <div className={classes.roadMapList}>
              <div className={classes.roadMapRow}>
                <div className={`${classes.roadMapCol} ${classes.active}`}>
                  <h3 className={classes.year}>December</h3>
                  <div className={classes.monthItem}>
                    <h4>10TH</h4>
                    <p>Community event</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>12TH</h4>
                    <p>Marketing kickoff</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>20TH</h4>
                    <p>Public BATA</p>
                  </div>
                </div>
                <div className={`${classes.roadMapCol} ${classes.active}`}>
                  <h3 className={classes.year}>2022 Q1</h3>
                  <div className={classes.monthItem}>
                    <h4>JAN</h4>
                    <p>In APP trade function</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>FEB</h4>
                    <p>Marketplace</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>MAR</h4>
                    <p>IEO / IDO</p>
                  </div>
                </div>
                <div className={`${classes.roadMapCol} ${classes.active}`}>
                  <h3 className={classes.year}>2022 Q2</h3>
                  <div className={classes.monthItem}>
                    <h4>APR</h4>
                    <p>In-APP wallet upgrade</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>MAY</h4>
                    <p>Multi-Chain wallet upgrade</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>JUN</h4>
                    <p>Multi-Chain marketplace upgrade</p>
                  </div>
                </div>
                <div className={classes.roadMapCol}>
                  <h3 className={classes.year}>2022 Q3</h3>
                  <div className={classes.monthItem}>
                    <h4>JUL</h4>
                    <p>Achievement system</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>AUG</h4>
                    <p>Quest system</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>SEP</h4>
                    <p>Rental system</p>
                  </div>
                </div>
                <div className={classes.roadMapCol}>
                  <h3 className={classes.year}>2022 Q4</h3>
                  <div className={classes.monthItem}>
                    <h4>OCT</h4>
                    <p>Online marathon</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>NOV</h4>
                    <p>Sociaal-Fi element</p>
                  </div>
                  <div className={classes.monthItem}>
                    <h4>DEC</h4>
                    <p>Community event</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={classes.footer}>
        <div className='container'>
          <div className={classes.footerInner}>
              <div className={classes.footerLogo}>
                <Link href="/">
                  <a><Image
                  src={Logo}
                  width={150} height={50}							
                  alt="Thumbs" /></a>
                </Link>
              </div>
              <div className={classes.copyright}>
                <span>© 2022 NFINITE</span>
              </div>
              <div className={classes.footerLink}>
                <ul className={`list-unstyled` + ' ' + classes.footerList}>
                  <li>
                  <Link href="/"><a>Privacy Policy</a></Link>
                  </li>
                  <li>
                  <Link href="/"><a>Terms of Service</a></Link>
                  </li>
                </ul>
              </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Footer;
