
import React, {useState, useEffect} from 'react';
import Link from "next/link";
import Image from "next/image";

import { useWeb3 } from "@3rdweb/hooks";

import Logo from 'public/main/images/logo.svg';
import classes from './header.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";


import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { faAngleLeft, faArrowRightFromBracket } from '@fortawesome/free-solid-svg-icons';
library.add(faAngleLeft, faArrowRightFromBracket);



import Buttons from 'public/account/Buttons.module.scss';

const Header = () => {

    const { address, connectWallet, disconnectWallet } = useWeb3();
    const [isActive, setActive] = useState("false");
    function handleClick() {
        setActive(isActive => !isActive);
    }
    let toggleCheck = isActive ? '':'active';


    const [scroll, setScroll] = useState(false)
    useEffect(() => {
      window.addEventListener("scroll", () => {
        setScroll(window.scrollY > 10)
      })
    }, [])





  return (
    <>
        <header id="header" className={scroll ? "headerWraps scrolled" : "headerWraps"}>
            <div className='container'>
                <div className={classes.mainHeader}>
                    <div className={classes.logoWrap}>
                        <div className={classes.menuIcon} onClick={handleClick}>
                            <span className={classes.menuIcons}>
                            <FontAwesomeIcon icon={faBars}/>
                            </span>
                        </div>
                        <div className={classes.logo}>
                            <Link href="/">
                            <a><Image
                            src={Logo}
                            width={150} height={50}							
                            alt="Thumbs" /></a>
                            </Link>
                        </div>
                    </div>
                    <div className={`${classes.menuList}  menuList ${toggleCheck}`} >
                        <ul className={`list-unstyled` + ' ' + classes.headerList}>
                            <li className={classes.closeMenu } onClick={handleClick}>
                                <FontAwesomeIcon icon={faTimes} size="2x" />
                            </li>
                            <li>
                                <Link  href="/">
                                    <a className={classes.active}>
                                        Home
                                    </a>							
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>
                                        About
                                    </a>							
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>
                                        Roadmap
                                    </a>							
                                </Link>
                            </li>
                            <li>
                                <Link className={classes.thumbsItem} href="#">
                                    <a >
                                        FAQs
                                    </a>							
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                {address ? (
                                    <div>
                                    <span className="mataaddress">Hello, <span className="walletID">{address.substring(0, 5)}…{address.substring(address.length - 4)}</span></span>
                                        <Link href="/">
                                        <a className={`btn ${Buttons.btnDanger}`} onClick={() => disconnectWallet("injected")}>Disconnect</a>
                                        </Link>	
                                    </div>
                                ) : (
                                    <Link href="/">
                                    <a className={`btn ${classes.btn} ${Buttons.btnOutlinePrimary}`} onClick={() => connectWallet("injected")} >
                                        Connect Wallet
                                    </a>
                                    </Link>
                                )}
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </>
  );
}

export default Header;
