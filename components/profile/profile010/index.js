import React, { useState, useEffect } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";
import {Button, Modal} from 'react-bootstrap';
import Modals from '../../../public/account/Modals.module.scss';
import Form from '../../../public/account/Form.module.scss';

import classes from "/public/profile/profile010/index.module.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faClone, faImage, faFlag, faAngleDoubleDown,faAngleDoubleUp, faBars, faDiamond } from "@fortawesome/free-solid-svg-icons";
import Thumbs010 from "@/components/thumbnails/thumbnails010";

import downArrow from '/public/profile/profile010/images/down-arrow.svg';
import profileLogo from "/public/profile/profile010/images/profile.png";



library.add(faClone, faImage);

import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

export default function Profile010() {
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
  useEffect(() => {
    if (typeof document !== undefined) {
      require("bootstrap/dist/js/bootstrap");
    }
  }, []);
  useEffect(() => {
    document.querySelector("body").classList.add("profile10");
  });
  const [isCopied, setIsCopied] = useState(false);
  const onCopyText = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };
  const codeSnitted = `0xceB945...Bd3c8D5D`;

  return (
    <>
      <div className={classes.profileWrap}>
        
        <div className={`container` + ' ' + classes.container}>
          <div className={classes.bannerBgImg}>
            <div className={`row` + ' ' + classes.userWrap}>
              <div className={`col-md-3` + ' ' + classes.profileImg}>
                <Image
                  className={`col-md-6` + ' ' + classes.profilePicture}
                  src={profileLogo}
                  width={256} height={256}              
                  alt="Thumbs"
                />
              </div>
              <div className={classes.userInfo}>
                <h2 className={classes.userMore}>@Luffyonepiece</h2>
                <h1 className={classes.userName}>Monkey D. Luffy</h1>
                <div className={classes.copySocialWrap}>
                  <div className={classes.userId}>
                    <CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
                      <div className={classes.copyWrap}>
                        {codeSnitted}                    
                        <span className={classes.copyTxt}>                      
                          {isCopied ? (
                            <span>0xceB945...Bd3c8D5D</span>
                          ) : (
                            <FontAwesomeIcon icon={faClone} />
                          )}
                        </span>
                      </div>
                    </CopyToClipboard>
                  </div>
                  <div className={classes.socialList}>
                    <Link href="#"><a>
                      <FontAwesomeIcon icon={faTwitter} size="lg" />
                    </a>
                    </Link>
                    <Link href="#"><a>
                      <FontAwesomeIcon icon={faInstagram} size="lg" />
                    </a>
                    </Link>
                    <Link href="#"><a>
                      <FontAwesomeIcon icon={faFacebook} size="lg" />
                    </a>
                    </Link>
                  </div>
                </div>
                <p className={classes.userDesc}>
                  SoundMint curated NFTs are generative music collectibles that combine generative visuals with generative music by pairing stems to visual layers; allowing ... 
                </p>                    
              </div>
            </div>               
          </div>
        </div>

        <div className={`container` + ' ' + classes.container}>
          <div className={classes.tabWrap}>
            <div className={`nav` + ' ' + classes.nav} 
              role="tablist"
              aria-orientation="vertical" >
      
                <button className={`btn`} data-bs-toggle="pill" data-bs-target="#collectionWrap"
                aria-controls="v-pills-messages"
                role="tab" aria-selected="false"
                type="button">                  
                  99  Collections
                </button>
        
                <button className={`btn` } data-bs-toggle="pill"
                data-bs-target="#exhibitionWrap"
                aria-controls="v-pills-messages"
                role="tab" aria-selected="false" 
                type="button">
                  99  Exhibitions
                </button>

                <button className={`btn active` }  data-bs-toggle="pill"
                data-bs-target="#assetsWrap"
                aria-controls="v-pills-messages"
                role="tab" aria-selected="true"                     
                type="button">
                  99 Assets
                </button>
        
            </div>
            <div className={classes.likeWrap }>
              <p className={classes.statusWrap}>
                <span><span className={classes.num}>40</span> Collection Likes</span>
                <span><span className={classes.num}>40</span> Exhibition Likes</span>
              </p>
            </div>
          </div>
          <div className={classes.profileContents}>            
            <Thumbs010 />           
          </div>
          
        </div>

        <footer className={classes.footer}>
          <div className='container'>
            <div className={classes.footerRow}>
              <div className={classes.copyright}>Copyright 2022 ©username. All rights reserved   | Powered by NFTIFY <Image src={downArrow} alt='Down Logo' width={50} height={30} /> </div>
              <ul className={classes.socialContainer}>
                <li>
                  Joined:  November 2021
                </li>
                <li><a className={Modals.openPopup} onClick={handleShow}><FontAwesomeIcon icon={faFlag} size="lg" /> Report</a></li>
                <li>
                  <span>Social Follow :</span>
                  <Link href='/'><a>
                    <FontAwesomeIcon icon={faTwitter} size="lg" />
                  </a></Link>
                </li>
                <li>
                  <Link href='/'><a>
                    <FontAwesomeIcon icon={faFacebook} size="lg" />
                  </a></Link>
                </li>
                <li>
                  <Link href='/'><a>
                    <FontAwesomeIcon icon={faInstagram} size="lg" />
                  </a></Link>
                </li>
              </ul>

            </div>
          </div>
        </footer>
      </div>
	  <Modal show={show} onHide={handleClose}>
		<Modal.Header closeButton>
		</Modal.Header>
		<Modal.Body>
			<h1 className={Modals.bodySubtitles}>Report this item</h1>
			<div className={` ${Form.formGroup}`}>
				<label>I think this item is...</label>
				<select className={`form-select form-moreselect ${Form.selectDrop}`}>
					<option>Fake collection or possible scam</option>
					<option>Spam</option>
					<option>Other</option>
				</select>
			</div>
		</Modal.Body>
	</Modal>
    </>
  );
}
