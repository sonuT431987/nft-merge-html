import React, { useState, useEffect } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";
import classes from './Profile003.module.scss';
import {Button, Modal} from 'react-bootstrap';
import Modals from '../../../public/account/Modals.module.scss';
import Form from '../../../public/account/Form.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faImage, faFlag } from "@fortawesome/free-regular-svg-icons";
import Thumbs003 from "../../../components/thumbnails/thumbnails003";

import downArrow from '../../../public/profile/profile002/images/down-arrow.svg';

import profileBG from "../../../public/profile/profile003/images/profilebg.png";
import profileLogo from "../../../public/profile/profile003/images/profile.png";


library.add(faClone, faImage);

import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

export default function Profile003() {
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
  useEffect(() => {
    if (typeof document !== undefined) {
      require("bootstrap/dist/js/bootstrap");
    }
  }, []);
  useEffect(() => {
    document.querySelector("body").classList.add("profile3");
  });
  const [isCopied, setIsCopied] = useState(false);
  const onCopyText = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };
  const codeSnitted = `0xceB945...Bd3c8D5D`;
  return (
    <>
	<div className={classes.profileWrap}>
		<div className={classes.bannerWrap}>
		<div className={classes.bannerImg}>
			<Image src={profileBG} alt="Banner Image" width={2000} height={360} />
		</div>
		<div className="container">
			<div className="row">
			<div className="col-md-4">
				<div className={classes.userWrap}>
					<div className={classes.profileImg}>
					<Image
					className={classes.profilePicture}
					src={profileLogo}
					height={256}
					width={256}
					alt="Thumbs"
					/>
				</div>
				<div className={classes.userInfo}>
					<h1 className={classes.userName}>Little Monkey 🍌</h1>
					<h2 className={classes.userMore}>@Deekaykwon</h2>
					<div className={classes.userId}>
					<CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
						<div className={classes.copyWrap}>
						{codeSnitted}
						<span className={classes.copyTxt}>
							{isCopied ? (
							<span>Copied!</span>
							) : (
							<FontAwesomeIcon icon={faClone} />
							)}
						</span>
						</div>
					</CopyToClipboard>
					</div>
					<p className={classes.socialList}>
						<Link href="/"><a>
						<FontAwesomeIcon icon={faTwitter} size="lg" />
						</a>
						</Link>
						<Link href="/"><a>
						<FontAwesomeIcon icon={faInstagram} size="lg" />
						</a>
						</Link>
						<Link href="/"><a>
						<FontAwesomeIcon icon={faFacebook} size="lg" />
						</a>
						</Link>
					</p>
				<div className={classes.likeWrap }>
					<p className={classes.statusWrap}>
						<span><span className={classes.num}>99</span><br/>Collection Likes</span>
						<span><span className={classes.num}>99</span><br/>Exhibition Likes</span>
					</p>
					<p className={classes.userDesc}>
						Lorem Ipsum is simply dummy text of the printing and
						typesetting dvvindustry. Lorem Ipsum is simply dummy text of
						the printing and typesetting dvvindustry. Lorem Ipsum has
						been dummy text of the printing and typesetting dvvindustry.
						Lorem Ipsum has been
					</p>
					</div>
				</div>
				</div>
				<div
				className={`nav flex-column ${classes.navTabs}`} role="tablist"
				aria-orientation="vertical"
				>
				<button
					className={`nav-link active ${classes.navTabsItems}`}
					data-bs-toggle="pill"
					data-bs-target="#collectionWrap"
					type="button"
					role="tab"
					aria-selected="true"
				>
					99 Collections
				</button>
				<button
					className={`nav-link ${classes.navTabsItems}`}
					data-bs-toggle="pill"
					data-bs-target="#spacesWrap"
					type="button"
					role="tab"
					aria-selected="false"
				>
					99 Exhibitions
				</button>
				<button
					className={`nav-link ${classes.navTabsItems}`}
					data-bs-toggle="pill"
					data-bs-target="#assetsAll"
					type="button"
					role="tab"
					aria-selected="false"
				>
					99 All Assets
				</button>
				</div>
			</div>
			<div className="col-md-8">
				<Thumbs003 />
			</div>
			</div>
		</div>
		</div>
		<footer className={classes.footer}>
			<div className='container'>
				<div className={classes.footerRow}>
					<div className={classes.copyright}>Powered by NFTIFY <Image src={downArrow} alt='Down Logo' width={50} height={30} /> </div>
					<ul className={classes.socialContainer}>
						<li>
							Joined:  November 2021
						</li>
						<li><a className={Modals.openPopup} onClick={handleShow}><FontAwesomeIcon icon={faFlag} size="lg" /> Report</a></li>
						<li>
							<span>Social Follow :</span>
							<Link href='/'><a>
								<FontAwesomeIcon icon={faTwitter} size="lg" />
							</a></Link>
						</li>
						<li>
							<Link href='/'><a>
								<FontAwesomeIcon icon={faFacebook} size="lg" />
							</a></Link>
						</li>
						<li>
							<Link href='/'><a>
								<FontAwesomeIcon icon={faInstagram} size="lg" />
							</a></Link>
						</li>
					</ul>

				</div>
			</div>
		</footer>
	</div>
	<Modal show={show} onHide={handleClose}>
    <Modal.Header closeButton>
    </Modal.Header>
    <Modal.Body>
        <h1 className={Modals.bodySubtitles}>Report this item</h1>
        <div className={` ${Form.formGroup}`}>
            <label>I think this item is...</label>
            <select className={`form-select form-moreselect ${Form.selectDrop}`}>
                <option>Fake collection or possible scam</option>
                <option>Spam</option>
                <option>Other</option>
            </select>
        </div>
    </Modal.Body>
</Modal>
    </>
  );
}
