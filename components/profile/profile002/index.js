import React, { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";

import {Button, Modal} from 'react-bootstrap';
import classes from './Profile002.module.scss';
import Modals from '../../../public/account/Modals.module.scss';
import Form from '../../../public/account/Form.module.scss';
import 'react-tabs/style/react-tabs.css';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faImage, faFlag } from "@fortawesome/free-regular-svg-icons";
import downArrow from '../../../public/profile/profile002/images/down-arrow.svg';
import Thumbs002 from "../../../components/thumbnails/thumbnails002"; 
// Layout Format

// Images Included need to remove Later
import profileBG from '../../../public/profile/profile002/images/profilebg.png';
import profileLogo from '../../../public/profile/profile002/images/profile.png';


library.add(faClone, faImage, faFlag);

import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";



export default function Profile002() {

    const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	
	const [isCopied, setIsCopied] = useState(false);
	const onCopyText = () => {
		setIsCopied(true);
		setTimeout(() => {
		setIsCopied(false);
		}, 1000);
	};
	const codeSnitted = `0xceB945...Bd3c8D5D`;
  return (
	<>
        <div className={classes.bannerWrap}>
          <div className={classes.bannerImg}>
            <Image
              src={profileBG}
              width={2000}
              height={360}
              alt="Banner Image"
            />
          </div>
          <div className={classes.userWrap}>
            <Image
              className={classes.profilePicture}
              src={profileLogo}
              height={250}
              width={250}
              alt="Thumbs"
            />
            <div className={classes.userInfo}>
				<div className="container">
					<h1 className={classes.userName}>Deekay Kwon 💎</h1>
					<h6 className={classes.userMore}>@Deekaykwon</h6>
					<div className={classes.userId}>
						<CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
						<div className={classes.copyWrap}>
							{codeSnitted}
							<span className={classes.copyTxt}>
							{isCopied ? (
								<span>Copied!</span>
							) : (
								<FontAwesomeIcon icon={faClone} />
							)}
							</span>
						</div>
						</CopyToClipboard>
					</div>
					<div className={classes.statusWrap}>
						<h5>99 <span>Collection Likes</span></h5>
						<h5>99K <span>Likes</span></h5>
					</div>
					<h5 className={classes.userDesc}>
						Lorem Ipsum is simply dummy text of the printing and typesetting
						dvvindustry. Lorem Ipsum is simply dummy text of the printing
						and typesetting dvvindustry. Lorem Ipsum has been dummy text of
						the printing and typesetting dvvindustry. Lorem Ipsum has been
					</h5>
					<p className={classes.socialList}>
						<Link href="/"><a>
						<FontAwesomeIcon icon={faTwitter} size="lg" />
						</a>
						</Link>
						<Link href="/"><a>
						<FontAwesomeIcon icon={faInstagram} size="lg" />
						</a>
						</Link>
						<Link href="/"><a>
						<FontAwesomeIcon icon={faFacebook} size="lg" />
						</a>
						</Link>
					</p>
				</div>
			</div>
          </div>
        </div>
		<Thumbs002 />
		<footer className={classes.footer}>
			<div className='container'>
				<div className={classes.footerRow}>
					<div className={classes.copyright}>Powered by NFTIFY <Image src={downArrow} alt='Down Logo' width={50} height={30} /> </div>
					<ul className={classes.socialContainer}>
						<li>
							Joined:  November 2021
						</li>
						<li><a className={Modals.openPopup} onClick={handleShow}><FontAwesomeIcon icon={faFlag} size="lg" /> Report</a></li>
						<li>
							<span>Social Follow :</span>
							<Link href='/'><a>
								<FontAwesomeIcon icon={faTwitter} size="lg" />
							</a></Link>
						</li>
						<li>
							<Link href='/'><a>
								<FontAwesomeIcon icon={faFacebook} size="lg" />
							</a></Link>
						</li>
						<li>
							<Link href='/'><a>
								<FontAwesomeIcon icon={faInstagram} size="lg" />
							</a></Link>
						</li>
					</ul>

				</div>
			</div>
		</footer>

		<Modal show={show} onHide={handleClose}>
			<Modal.Header closeButton>
			</Modal.Header>
			<Modal.Body>
				<h1 className={Modals.bodySubtitles}>Report this item</h1>
				<div className={` ${Form.formGroup}`}>
					<label>I think this item is...</label>
					<select className={`form-select form-moreselect ${Form.selectDrop}`}>
						<option>Fake collection or possible scam</option>
						<option>Spam</option>
						<option>Other</option>
					</select>
				</div>
			</Modal.Body>
		</Modal>
	</>
  )
}