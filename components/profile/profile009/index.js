import React, { useState, useEffect } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";
import {Button, Modal} from 'react-bootstrap';
import Modals from '../../../public/account/Modals.module.scss';
import Form from '../../../public/account/Form.module.scss';

import classes from "/public/profile/profile009/Index.module.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faClone, faImage, faFlag, faDiamond } from "@fortawesome/free-solid-svg-icons";
import Thumbs009 from "@/components/thumbnails/thumbnails009";

import downArrow from '/public/profile/profile009/images/down-arrow.svg';
import profileLogo from "/public/profile/profile009/images/profile.png";
import profileSidebar from "/public/profile/profile009/images/profileSidebar.png";


library.add(faClone, faImage);

import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

export default function Profile009() {
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

  useEffect(() => {
    if (typeof document !== undefined) {
      require("bootstrap/dist/js/bootstrap");
    }
  }, []);
  useEffect(() => {
    document.querySelector("body").classList.add("profile9");
  });
  const [isCopied, setIsCopied] = useState(false);
  const onCopyText = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };
  const codeSnitted = `0xceB945...Bd3c8D5D`;

  return (
    <>
      <div className={classes.profileWrap}>
        <div className={`container` + ' ' + classes.container}>

 
          <div className={classes.profileContents}>
            <div className="row">
              
              <div className="col-md-4">
                <div className={classes.sidebarImg}>
                  <Image
                    className={classes.sidebarProfile}
                    src={profileSidebar}
                    width={512} height={812}              
                    alt="Thumbs"
                  />
                  <div className={classes.likeWrap }>
                    <p className={classes.statusWrap}>
                      <span><span className={classes.num}>40</span> Collection Likes</span>
                      <span><span className={classes.num}>400</span> Exhibition Likes</span>
                    </p>
                  </div>
                  
                </div>
                <div className={classes.userWrap}>
                  <div className={classes.profileImg}>
                    <Image
                      className={classes.profilePicture}
                      src={profileLogo}
                      width={512} height={812}              
                      alt="Thumbs"
                    />
                  </div>
                  <div className={classes.userInfo}>
                    
                    <h1 className={classes.userName}>Jenny Wilson </h1>  
                    <h2 className={classes.userMore}>@JennyWilson</h2>                
                    <div className={classes.userId}>
                      <CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
                        <div className={classes.copyWrap}>
                        <span className={classes.copyIcon}><FontAwesomeIcon icon={faDiamond} /></span> 
                          {codeSnitted}                    
                          <span className={classes.copyTxt}>                      
                            {isCopied ? (
                              <span>0xf7A81...613Ff</span>
                            ) : (
                              <FontAwesomeIcon icon={faClone} />
                            )}
                          </span>
                        </div>
                      </CopyToClipboard>
                    </div>
                    <p className={classes.userDesc}>
                    Lorem Ipsum is simply dummy text of th...
                    </p>
                    <div className={classes.socialList}>
                      <Link href="#"><a>
                        <FontAwesomeIcon icon={faTwitter} size="lg" />
                      </a>
                      </Link>
                      <Link href="#"><a>
                        <FontAwesomeIcon icon={faInstagram} size="lg" />
                      </a>
                      </Link>
                      <Link href="#"><a>
                        <FontAwesomeIcon icon={faFacebook} size="lg" />
                      </a>
                      </Link>
                    </div>    
                  
                  </div>
                </div>
              </div>
              <div className={`col-md-8` + ' ' + classes.mobOrder2}>
              <div className={classes.tabWrap}>
                  <div className={`nav` + ' ' + classes.nav} 
                    role="tablist"
                    aria-orientation="vertical" >
            
                      <a className={`nav-link active`} data-bs-toggle="pill" data-bs-target="#collectionWrap"
                      aria-controls="v-pills-messages"
                      role="tab" aria-selected="false"
                      type="button">                  
                        99  Collections
                      </a>
              
                      <a className={`nav-link` } data-bs-toggle="pill"
                      data-bs-target="#exhibitionWrap"
                      aria-controls="v-pills-messages"
                      role="tab" aria-selected="false" 
                      type="button">
                        99  Exhibitions
                      </a>

                      <a className={`nav-link` }  data-bs-toggle="pill"
                      data-bs-target="#assetsWrap"
                      aria-controls="v-pills-messages"
                      role="tab" aria-selected="true"                     
                      type="button">
                        99 Assets
                      </a>
              
                  </div>
                </div>
                <Thumbs009 />
              </div>
            </div>
          </div>
          
        </div>

        <footer className={classes.footer}>
          <div className='container'>
            <div className={classes.footerRow}>
              <div className={classes.copyright}>Copyright 2022 ©username. All rights reserved   | Powered by NFTIFY <Image src={downArrow} alt='Down Logo' width={50} height={30} /> </div>
              <ul className={classes.socialContainer}>
                <li>
                  Joined:  November 2021
                </li>
                <li><a className={Modals.openPopup} onClick={handleShow}><FontAwesomeIcon icon={faFlag} size="lg" /> Report</a></li>
                <li>
                  <span>Social Follow :</span>
                  <Link href='/'><a>
                    <FontAwesomeIcon icon={faTwitter} size="lg" />
                  </a></Link>
                </li>
                <li>
                  <Link href='/'><a>
                    <FontAwesomeIcon icon={faFacebook} size="lg" />
                  </a></Link>
                </li>
                <li>
                  <Link href='/'><a>
                    <FontAwesomeIcon icon={faInstagram} size="lg" />
                  </a></Link>
                </li>
              </ul>

            </div>
          </div>
        </footer>
      </div>
	  <Modal show={show} onHide={handleClose}>
		<Modal.Header closeButton>
		</Modal.Header>
		<Modal.Body>
			<h1 className={Modals.bodySubtitles}>Report this item</h1>
			<div className={` ${Form.formGroup}`}>
				<label>I think this item is...</label>
				<select className={`form-select form-moreselect ${Form.selectDrop}`}>
					<option>Fake collection or possible scam</option>
					<option>Spam</option>
					<option>Other</option>
				</select>
			</div>
		</Modal.Body>
	</Modal>
    </>
  );
}
