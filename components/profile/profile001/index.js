import React, { useState, useEffect } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";

import classes from "/public/profile/profile001/Index.module.scss";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faImage, faFlag, faWallet } from "@fortawesome/free-solid-svg-icons";

import Collections001 from '@/components/thumbnails/collections/thumb001';
import Exhibitions001 from '@/components/thumbnails/exhibitions/thumb001';
import Assets001 from '@/components/thumbnails/assets/thumb001';

import downArrow from '/public/profile/profile001/images/down-arrow.svg';
import profileLogo from '/public/profile/profile001/images/profile.png';
import profileBG from '/public/profile/profile001/images/profilebg.png';

library.add(faClone, faImage);
import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

export default function Profile001() {

	useEffect(() => {
    if (typeof document !== undefined) {
      require("bootstrap/dist/js/bootstrap");
    }
  }, []);
  useEffect(() => {
    document.querySelector("body").classList.add("profile1");
  });
  const [isCopied, setIsCopied] = useState(false);
  const onCopyText = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };
  const codeSnitted = `0xceB945...Bd3c8D5D`;

	return (
		<>
			<div className={classes.profileWrap}>
        {/*=========  Banner Section  ==========*/}
        <div className={`container` + ' ' + classes.container}> 
          <div className={classes.banner}>
            <div className={`` + ' ' + classes.bannerImg}>
                <Image src={profileBG} alt="Banner Image" width={1600} height={250} />
            </div>
          </div>
        </div>

        {/*=========  Content Section  ==========*/}
        <div className={`container` + ' ' + classes.container}> 
          <div className="row">
            <div className="col-md-3">
              <div className={classes.userWrap}>
                <div className={classes.profileImg}>
                  <Image
                  className={classes.profilePicture}
                  src={profileLogo}
                  width={256} height={256}              
                  alt="Thumbs"
                  />
                </div>
                <div className={classes.userInfo}>
                  <h2 className={classes.userName}>Jenny Wilson ❄️</h2>
                  <h6 className={classes.userMore}>@JennyWilson</h6>
                  <div className={classes.likeWrap }>
                    <div className={classes.statusWrap}>
                      <div><span className={classes.num}>9.99k</span> Collection Likes</div>
                      <div><span className={classes.num}>9.99k</span> Exhibition Likes</div>
                    </div>
                  </div>
                  <div className={classes.userDesc}>
                    Lorem Ipsum is simply dummy text of the printing and typesetting
                    dvvindustry. Lorem Ipsum is simply dummy text of the printing
                    and typesetting dvvindustry. Lorem Ipsum has been dummy text of
                    the printing and typesetting dvvindustry. Lorem Ipsum has been
                  </div>
                  <div className={classes.userId}>
                    <span className={classes.walletLbl}>Wallet:</span>
                    <CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
                    <div className={classes.listItem + ' ' + classes.copyWrap}>
                      {codeSnitted}
                      <span className={classes.copyTxt}>
                      {isCopied ? (
                        <span>Copied!</span>
                      ) : (
                        <FontAwesomeIcon icon={faClone} />
                      )}
                      </span>
                    </div>
                    </CopyToClipboard>
                  </div>
                  <div className={classes.socialList}>
                    <span className={classes.socialLbl}>Connect:</span>
                    <Link href="#"><a className={classes.listItem}>
                    <FontAwesomeIcon icon={faTwitter} size="lg" />
                     adamconstable7
                    </a>
                    </Link>
                    <Link href="#"><a className={classes.listItem}>
                      <FontAwesomeIcon icon={faInstagram} size="lg" /> adamconstable7 
                    </a>
                    </Link>
                    <Link href="#"><a className={classes.listItem}>
                    <FontAwesomeIcon icon={faFacebook} size="lg" /> adamconstable7
                    </a>
                    </Link>
                  </div>
                  <div className={classes.joinedWrap}>
                    <span className={classes.joinLbl}>Joined:</span>
                    <span className={classes.joinDate}>November 2021</span>
                  </div>
                  <div className={classes.reportLbl}><FontAwesomeIcon icon={faFlag} size="lg" /> Report</div>
                </div>
              </div>
            </div>
            <div className="col-md-9">
              <div className={classes.tabWrap}>
                <div className={`nav` + ' ' + classes.nav} 
                  role="tablist"
                  aria-orientation="vertical" >      
                    <a className={`btn active`} data-bs-toggle="pill" data-bs-target="#collectionWrap"
                    aria-controls="v-pills-messages"
                    role="tab" aria-selected="true">                  
                      99  Collections
                    </a>        
                    <a className={`btn` } data-bs-toggle="pill"
                    data-bs-target="#exhibitionWrap"
                    aria-controls="v-pills-messages"
                    role="tab" aria-selected="false">
                      99  Exhibitions
                    </a>
                    <a className={`btn ` }  data-bs-toggle="pill"
                    data-bs-target="#assetsWrap"
                    aria-controls="v-pills-messages"
                    role="tab" aria-selected="false">
                      99 Assets
                    </a>        
                </div>
              </div>
              <div className={`tab-content` + ' ' + classes.tabContent} >
                <div className="tab-pane fade show active" id="collectionWrap">
                  <Collections001/>
                </div>
                <div className="tab-pane fade" id="exhibitionWrap">
                  <Exhibitions001 />
                </div>
                <div className="tab-pane fade " id="assetsWrap">
                  <Assets001 />
                </div>
              </div>
            </div>
          </div>
        </div>
		<div className={classes.fixIcons}>
			<span><button type="button" className={`btn w-100 ${classes.btnIcons}`}><FontAwesomeIcon icon={faWallet} size="lg" /></button></span>
		</div>

        {/*=========  Footer  ==========*/}
        <footer className={classes.footer}>
          <div className='container'>
            <div className={classes.footerRow}>
              <div className={`text-center` + ' ' + classes.copyright}>Copyright 2022 ©username. All rights reserved   | Powered by NFTIFY <Image src={downArrow} alt='Down Logo' width={50} height={30} /> </div>
            </div>
          </div>
        </footer>

      </div>
		</>
	)
}


