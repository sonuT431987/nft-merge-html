import React from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './assets001.module.scss';

import Thumbs from 'public/profile/profile001/images/thumbs.png';
import Nft from 'public/profile/profile001/images/nft.svg';

export default function Assets001() {
  return (
		<>
			<div className={classes.assetsAll}>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={512}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}>            
            <div className={classes.thumbTitlewrap}>
              <h3>
              <Link href="/"><a>NFT Name </a></Link>
              </h3>							
            </div>
            <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur ist at... </p>
            <div className={classes.nftChain}>
              <span className={classes.nftIcon}><Image
              src={Nft}
              width={12} height={12}						
              alt="Thumbs" /></span>	 NFT Chain 
            </div>								
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={512}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}>            
            <div className={classes.thumbTitlewrap}>
              <h3>
              <Link href="/"><a>NFT Name </a></Link>
              </h3>							
            </div>
            <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur ist at... </p>
            <div className={classes.nftChain}>
              <span className={classes.nftIcon}><Image
              src={Nft}
              width={12} height={12}						
              alt="Thumbs" /></span>	 NFT Chain 
            </div>								
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={512}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}>            
            <div className={classes.thumbTitlewrap}>
              <h3>
              <Link href="/"><a>NFT Name </a></Link>
              </h3>							
            </div>
            <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur ist at... </p>
            <div className={classes.nftChain}>
              <span className={classes.nftIcon}><Image
              src={Nft}
              width={12} height={12}						
              alt="Thumbs" /></span>	 NFT Chain 
            </div>								
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={512}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}>            
            <div className={classes.thumbTitlewrap}>
              <h3>
              <Link href="/"><a>NFT Name </a></Link>
              </h3>							
            </div>
            <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur ist at... </p>
            <div className={classes.nftChain}>
              <span className={classes.nftIcon}><Image
              src={Nft}
              width={12} height={12}						
              alt="Thumbs" /></span>	 NFT Chain 
            </div>								
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={512}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}>            
            <div className={classes.thumbTitlewrap}>
              <h3>
              <Link href="/"><a>NFT Name </a></Link>
              </h3>							
            </div>
            <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur ist at... </p>
            <div className={classes.nftChain}>
              <span className={classes.nftIcon}><Image
              src={Nft}
              width={12} height={12}						
              alt="Thumbs" /></span>	 NFT Chain 
            </div>								
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={512}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}>            
            <div className={classes.thumbTitlewrap}>
              <h3>
              <Link href="/"><a>NFT Name </a></Link>
              </h3>							
            </div>
            <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur ist at... </p>
            <div className={classes.nftChain}>
              <span className={classes.nftIcon}><Image
              src={Nft}
              width={12} height={12}						
              alt="Thumbs" /></span>	 NFT Chain 
            </div>								
          </div>
        </div>
			</div>
      <div className={classes.paginationWrap}>
        <ul>
          <li className={classes.active}><Link href="#" ><a>1</a></Link></li>
          <li><Link href="#" ><a>2</a></Link></li>
          <li><Link href="#" ><a>3</a></Link></li>
        </ul>
      </div>
		</>
  );
}

