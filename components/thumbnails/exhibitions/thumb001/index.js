import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './exhibitions001.module.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import Thumbs from 'public/profile/profile001/images/thumbs2.png';


export default function Exhibitions001() {
      return (
		<>
			<div className={classes.exhibitionWraps}>
				<div className={classes.thumblist}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="#">
					<a>
						<Image
						src={Thumbs}
						width={920} height={373}							
						alt="Thumbs" />
					</a>							
					</Link>
					{/* <div className={classes.wishLike}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
					</div> */}
					</div>
					<div className={classes.thumbInfo}>            
					<div className={classes.thumbTitlewrap}>
						<h3>
						<Link href="/"><a>Exhibition Name </a></Link>
						</h3>							
					</div>
					<p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Fauc ibus morbi fusce rutrum vestibulum pellentesque augue convallis hendrerit adipiscing. Mauris sed sed dignissim in vel egestas tris...</p>
					<div className={classes.descWishWrap}>
						<div className={classes.assetNum}>
						<span className={classes.num}>32 </span> Assets
						</div>
						<div className={classes.wish}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						<span className={classes.num}> 100 </span>								
						</div>
					</div>								
					</div>
				</div>
				<div className={classes.thumblist}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="#">
					<a>
						<Image
						src={Thumbs}
						width={920} height={373}							
						alt="Thumbs" />
					</a>							
					</Link>
					{/* <div className={classes.wishLike}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
					</div> */}
					</div>
					<div className={classes.thumbInfo}>            
					<div className={classes.thumbTitlewrap}>
						<h3>
						<Link href="/"><a>Exhibition Name </a></Link>
						</h3>							
					</div>
					<p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Fauc ibus morbi fusce rutrum vestibulum pellentesque augue convallis hendrerit adipiscing. Mauris sed sed dignissim in vel egestas tris... </p>
					<div className={classes.descWishWrap}>
						<div className={classes.assetNum}>
						<span className={classes.num}>32 </span> Assets
						</div>
						<div className={classes.wish}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						<span className={classes.num}> 100 </span>								
						</div>
					</div>								
					</div>
				</div>
				
			</div>
			<div className={classes.paginationWrap}>
				<ul>
					<li className={classes.active}><Link href="#" ><a>1</a></Link></li>
					<li><Link href="#" ><a>2</a></Link></li>
					<li><Link href="#" ><a>3</a></Link></li>
				</ul>
			</div>
		</>
  );
}

