import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import classes from './Collections007.module.scss';

import Thumb from '/public/profile/profile007/images/collectionthumb.png';


export default function Collections007() {

    const settings = {
      dots: false,
      arrows:true,
      autoplay:true,
      lazyLoad: true,
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      //variableWidth: true,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
        slidesToShow: 2,
        variableWidth: false,
        }
      }
      ]
    };

    return (
		<>

			<div className={classes.collectionWraps + ' ' + classes.slickSlider}>
        <Slider {...settings}>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={599} height={599}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbInfoInner}> 
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>         
                <div className={classes.thumbTitlewrap}>
                  <h3>
                  <Link href="/"><a>Collection Name ...</a></Link>
                  </h3>							
                </div>
                <div className={classes.thumbAsset}>
                  <span className={classes.num}>32</span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus...</p>
                <div className={classes.descWishWrap}>
                  <div className={classes.wish}>
                    <span className={classes.num}>100 </span>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  </div>
                </div>
              </div>								
            </div>
          </div>	
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={599} height={599}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbInfoInner}> 
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>         
                <div className={classes.thumbTitlewrap}>
                  <h3>
                  <Link href="/"><a>Collection Name ...</a></Link>
                  </h3>							
                </div>
                <div className={classes.thumbAsset}>
                  <span className={classes.num}>32</span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus...</p>
                <div className={classes.descWishWrap}>
                  <div className={classes.wish}>
                    <span className={classes.num}>100 </span>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  </div>
                </div>
              </div>								
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={599} height={599}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbInfoInner}> 
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>         
                <div className={classes.thumbTitlewrap}>
                  <h3>
                  <Link href="/"><a>Collection Name ...</a></Link>
                  </h3>							
                </div>
                <div className={classes.thumbAsset}>
                  <span className={classes.num}>32</span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus...</p>
                <div className={classes.descWishWrap}>
                  <div className={classes.wish}>
                    <span className={classes.num}>100 </span>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  </div>
                </div>
              </div>								
            </div>
          </div>
        </Slider>
	


			</div>



		</>
  );
}

