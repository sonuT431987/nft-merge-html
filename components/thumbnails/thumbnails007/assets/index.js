import React from "react";
import Image from "next/image";
import Link from "next/link";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import classes from './Assets007.module.scss';

import Thumb from '/public/profile/profile007/images/assetsthumb.png';

export default function Assets007() {
  const settings = {
    infinite: true,
    slidesToShow: 2.5,
    speed: 500,
    rows: 2,
    slidesPerRow: 2,
    //vertical: true,
    //verticalSwiping: true,
    arrows:true,

		responsive: [
		{
			breakpoint: 1024,
			settings: {
        slidesPerRow: 1,
        slidesToShow: 4,
      }
    },
    {
			breakpoint: 550,
			settings: {
        slidesPerRow: 1,
        slidesToShow: 2,
      }
    } 
		]
  };
  return (
		<>	

			<div className={classes.assetsAll + ' ' + classes.slickSlider} >
        <Slider {...settings }>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumb}
                width={240} height={240}							
                alt="Thumbs" />
              </a>							
              </Link>
            </div>
          </div>
        </Slider>
        
	
			</div>


		</>
  );
}

