import React from 'react';
import Assets007 from './assets';
import Collections007 from './collections';
import Exhibitions007 from './exhibitions';
// import classes from './Thumbs007.module.scss'

const Thumbs007 = () => {
  return (
	<>
		<div className="tab-content">
			<div className="tab-pane fade show active" id="collectionWrap">
				<Collections007/>
			</div>
			<div className="tab-pane fade " id="exhibitionWrap">
				<Exhibitions007 />
			</div>
			<div className="tab-pane fade " id="assetsWrap">
				<Assets007 />
			</div>
		</div>
	
	</>
  );
}

export default Thumbs007;
