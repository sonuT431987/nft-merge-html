import React from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Collections003.module.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import Thumbs from '../../../../public/profile/profile003/images/thumbs.png';


export default function Collections003() {
      return (
		<>
		<div className={classes.collectionWraps}>
			<article className={classes.thumbsWrap}>
				<div className={classes.front}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
				</div>
				<div className={classes.back}>
					<div className={classes.wishLike}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
					</div>
					<h3>Collection Name</h3>
					<div className={classes.dateWish}>
						<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
						<div className={classes.wish}>
							<span className={classes.num}>100 </span>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>						
					</div>
				</div>
			</article>
			<article className={classes.thumbsWrap}>
			<div className={classes.front}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
				</div>
				<div className={classes.back}>
					<div className={classes.wishLike}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
					</div>
					<h3>Collection Name</h3>
					<div className={classes.dateWish}>
						<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
						<div className={classes.wish}>
							<span className={classes.num}>100 </span>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>						
					</div>
				</div>
			</article>
			<article className={classes.thumbsWrap}>
			<div className={classes.front}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
				</div>
				<div className={classes.back}>
					<div className={classes.wishLike}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
					</div>
					<h3>Collection Name</h3>
					<div className={classes.dateWish}>
						<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
						<div className={classes.wish}>
							<span className={classes.num}>100 </span>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>						
					</div>
				</div>
			</article>
			<article className={classes.thumbsWrap}>
			<div className={classes.front}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
				</div>
				<div className={classes.back}>
					<div className={classes.wishLike}>
						<FontAwesomeIcon icon={faHeart} className="wishicon"/>
					</div>
					<h3>Collection Name</h3>
					<div className={classes.dateWish}>
						<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
						<div className={classes.wish}>
							<span className={classes.num}>100 </span>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>						
					</div>
				</div>
			</article>
        </div>
		</>
  );
}

