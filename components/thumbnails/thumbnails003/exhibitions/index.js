import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Exhibitions003.module.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import thumbsFull from '../../../../public/profile/profile003/images/thumbsfull.png';


export default function Exhibitions002() {
      return (
		<>
			<div className={classes.spacesWrap}>
				<article className={classes.thumbsFullWrap}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={thumbsFull}
								height={643}
								width={1056}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Exhibitions<br/>Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<span className={classes.num}>100 </span>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
							</div>						
						</div>
					</div>
				</article>
				<article className={classes.thumbsFullWrap}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={thumbsFull}
								height={643}
								width={1056}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Exhibitions<br/>Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<span className={classes.num}>100 </span>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
							</div>						
						</div>
					</div>
				</article>
			</div>
		</>
  );
}

