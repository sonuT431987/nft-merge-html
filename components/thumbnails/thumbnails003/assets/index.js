import React from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Assets003.module.scss';

import Thumbs from '../../../../public/profile/profile003/images/thumbs.png';


export default function Assets003() {
      return (
		<>
			<div className={classes.assetsAll}>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={130}
								width={130}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>

				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<div className="w-100 d-none d-md-block"></div>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<div className="w-100 d-none d-md-block"></div>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
				<article className={classes.thumbsWrapAll}>
					<div className={classes.front}>
						<Link className={classes.thumbsItem} href="/">
							<a>
							<Image
								src={Thumbs}
								height={150}
								width={150}
								alt="Thumbs"
							/>
							</a>
						</Link>
					</div>
					<div className={classes.back}>
						<h3>NFT<br/>Name</h3>
					</div>
				</article>
			</div>
		</>
  );
}

