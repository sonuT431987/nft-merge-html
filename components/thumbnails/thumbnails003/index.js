import React from 'react';
import Link from "next/link";
import Assets003 from './assets';
import Collections003 from './collections';
import Exhibitions003 from './exhibitions';
import classes from './Thumbs003.module.scss'

const Thumbs003 = () => {
  return (
	<>
		<div className="tab-content">
			<div className="tab-pane fade show active" id="collectionWrap">
				<Collections003/>
			</div>
			<div className="tab-pane fade" id="spacesWrap">
				<Exhibitions003 />
			</div>
			<div className="tab-pane fade" id="assetsAll">
				<Assets003 />
			</div>
		</div>
		<div className={classes.paginationWrap}>
			<ul>
				<li className={classes.active}><Link href="/" ><a>1</a></Link></li>
				<li><Link href="/" ><a>2</a></Link></li>
				<li><Link href="/" ><a>3</a></Link></li>
			</ul>
		</div>	
	</>
  );
}

export default Thumbs003;
