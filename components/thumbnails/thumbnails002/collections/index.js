import React from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Collections002.module.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import Thumbs from '../../../../public/profile/profile002/images/thumbs.png';

export default function Collections002() {
      return (
		<>
		<div className={classes.collectionWraps}>
				<article className={classes.thumbsWrap}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					<div className={classes.thumbsItemContent}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Collection Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}>100 </span>
							</div>						
						</div>
					</div>
				</article>
				<article className={classes.thumbsWrap}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					<div className={classes.thumbsItemContent}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Collection Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}>100 </span>
							</div>						
						</div>
					</div>
				</article>
				<article className={classes.thumbsWrap}>
				<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					<div className={classes.thumbsItemContent}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Collection Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}>100 </span>
							</div>						
						</div>
					</div>
				</article>
				<article className={classes.thumbsWrap}>
				<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					<div className={classes.thumbsItemContent}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Collection Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}>100 </span>
							</div>						
						</div>
					</div>
				</article>
				<article className={classes.thumbsWrap}>
				<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					<div className={classes.thumbsItemContent}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Collection Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}>100 </span>
							</div>						
						</div>
					</div>
				</article>
				<article className={classes.thumbsWrap}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					<div className={classes.thumbsItemContent}>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
						<h3>Collection Name</h3>
						<div className={classes.dateWish}>
							<div className={classes.assetNum}><span className={classes.num}>32</span> Assets</div>
							<div className={classes.wish}>
								<FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}>100 </span>
							</div>						
						</div>
					</div>
				</article>
			</div>
		</>
  );
}

