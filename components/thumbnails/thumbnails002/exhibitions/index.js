import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Exhibitions002.module.scss';

import thumbsFull from '../../../../public/profile/profile002/images/thumbsfull.png';


export default function Exhibitions001() {
      return (
		<>
			<div className={classes.spacesWrap}>
				<article className={classes.thumbsWrap}>
					<Link className={classes.thumbsItem} href="/">
					<a>
						<div className={classes.thumbsContent}>
							<h3 className={classes.ItemTitle}>Space Name</h3>
						</div>
						<Image
						src={thumbsFull}
						height={900}
						width={1600}
						alt="Thumbs"
						/>
						</a>
					</Link>
				</article>
				<article className={classes.thumbsWrap}>
					<Link className={classes.thumbsItem} href="/">
					<a>
						<div className={classes.thumbsContent}>
							<h3 className={classes.ItemTitle}>Space Name</h3>
						</div>
						<Image
						src={thumbsFull}
						height={900}
						width={1600}
						alt="Thumbs"
						/>
						</a>
					</Link>
				</article>
			</div>
		</>
  );
}

