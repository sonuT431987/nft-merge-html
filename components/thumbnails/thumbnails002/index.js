import React from 'react';
import classes from './Thumbs002.module.scss';
import Link from "next/link";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

import Collection002 from './collections';
import Exhibitions002 from './exhibitions';
import Assets002 from './assets';


const Thumbs002 = () => {
  return (
	<>
		<div className="container">
			<div className={classes.tabsWrapper}>
				<Tabs>
					<TabList>
						<Tab>Collections</Tab>
						<Tab>Exhibitions</Tab>
						<Tab>All Assets</Tab>
					</TabList>
					<TabPanel>
						<Collection002 />
					</TabPanel>
					<TabPanel>
						<Exhibitions002/>
					</TabPanel>
					<TabPanel>
						<Assets002 />
					</TabPanel>
				</Tabs>
			</div>
			<div className={classes.paginationWrap}>
				<ul>
					<li className={classes.active}><Link href="/" ><a>1</a></Link></li>
					<li><Link href="/" ><a>2</a></Link></li>
					<li><Link href="/" ><a>3</a></Link></li>
				</ul>
			</div>
      	</div>	
	</>
  );
}

export default Thumbs002;
