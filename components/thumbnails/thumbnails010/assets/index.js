import React from "react";
import Image from "next/image";
import Link from "next/link";

import paging from '@/components/thumbnails/thumbnails010/Thumbs010.module.scss';
import classes from './Assets010.module.scss';

import Thumb from '../../../../public/profile/profile010/images/thumbAssets.png';


export default function Assets010() {

  return (
		<>	
			<div className={classes.assetsAll}>
	
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg} >
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumb}
              width={240} height={323}							
              alt="Thumbs" />
            </a>							
            </Link>
          </div>
          <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>3D Soft Curves</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>Esther Howard</p>		
            </div>
          </div>
        </div>
			

			</div>

      <div className={paging.paginationWrap}>
				<ul>
          <li><Link href="#" ><a className={paging.prev + ' ' + paging.disabled}>Prev</a></Link></li>
					<li><Link href="#" ><a>1</a></Link></li>
          <li className={paging.active}><span>...</span></li>
          <li><Link href="#" ><a>10</a></Link></li>
          <li><Link href="#" ><a>11</a></Link></li>
          <li className={paging.active}><Link href="#" ><a>12</a></Link></li>
          <li><Link href="#" ><a>16</a></Link></li>
          <li className={paging.active}><span>...</span></li>
					<li><Link href="#" ><a>26</a></Link></li>
					<li><Link href="#" ><a className={paging.next}>Next</a></Link></li>
				</ul>
			</div>


		</>
  );
}

