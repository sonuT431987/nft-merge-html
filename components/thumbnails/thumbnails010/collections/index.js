import React from "react";
import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import paging from '@/components/thumbnails/thumbnails010/Thumbs010.module.scss';
import classes from './Collections010.module.scss';
import Thumbs from '/public/profile/profile010/images/thumb.png'; 


export default function Collections010() {

    return (
		<>

			<div className={classes.collectionWraps}>
    
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={539}							
              alt="Thumbs" />
            </a>							
            </Link>						
          </div>
          <div className={classes.thumbInfo}>  
          
            <div className={classes.thumbInfoInner}>
              <div className={classes.wishLike}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              </div>
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>Collection<br/> Name </a></Link>
                </h3>							
              </div>
              <div className={classes.descWishWrap}>
                <div className={classes.assetNum}>
                  <span className={classes.num}>32 </span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                <div className={classes.wish}>
                   <span className={classes.num}> 100 </span>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  								
                </div>
              </div>
            </div>                
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={539}							
              alt="Thumbs" />
            </a>							
            </Link>						
          </div>
          <div className={classes.thumbInfo}>  
          
            <div className={classes.thumbInfoInner}>
              <div className={classes.wishLike}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              </div>
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>Collection<br/> Name </a></Link>
                </h3>							
              </div>
              <div className={classes.descWishWrap}>
                <div className={classes.assetNum}>
                  <span className={classes.num}>32 </span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                <div className={classes.wish}>
                   <span className={classes.num}> 100 </span>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  								
                </div>
              </div>
            </div>                
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={539}							
              alt="Thumbs" />
            </a>							
            </Link>						
          </div>
          <div className={classes.thumbInfo}>  
          
            <div className={classes.thumbInfoInner}>
              <div className={classes.wishLike}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              </div>
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>Collection<br/> Name </a></Link>
                </h3>							
              </div>
              <div className={classes.descWishWrap}>
                <div className={classes.assetNum}>
                  <span className={classes.num}>32 </span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                <div className={classes.wish}>
                   <span className={classes.num}> 100 </span>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  								
                </div>
              </div>
            </div>                
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={539}							
              alt="Thumbs" />
            </a>							
            </Link>						
          </div>
          <div className={classes.thumbInfo}>  
          
            <div className={classes.thumbInfoInner}>
              <div className={classes.wishLike}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              </div>
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>Collection<br/> Name </a></Link>
                </h3>							
              </div>
              <div className={classes.descWishWrap}>
                <div className={classes.assetNum}>
                  <span className={classes.num}>32 </span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                <div className={classes.wish}>
                   <span className={classes.num}> 100 </span>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  								
                </div>
              </div>
            </div>                
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={539}							
              alt="Thumbs" />
            </a>							
            </Link>						
          </div>
          <div className={classes.thumbInfo}>  
          
            <div className={classes.thumbInfoInner}>
              <div className={classes.wishLike}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              </div>
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>Collection<br/> Name </a></Link>
                </h3>							
              </div>
              <div className={classes.descWishWrap}>
                <div className={classes.assetNum}>
                  <span className={classes.num}>32 </span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                <div className={classes.wish}>
                   <span className={classes.num}> 100 </span>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  								
                </div>
              </div>
            </div>                
          </div>
        </div>
        <div className={classes.thumblist}>
          <div className={classes.thumbImg}>
            <Link className={classes.thumbsItem} href="#">
            <a>
              <Image
              src={Thumbs}
              width={512} height={539}							
              alt="Thumbs" />
            </a>							
            </Link>						
          </div>
          <div className={classes.thumbInfo}>  
          
            <div className={classes.thumbInfoInner}>
              <div className={classes.wishLike}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              </div>
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>Collection<br/> Name </a></Link>
                </h3>							
              </div>
              <div className={classes.descWishWrap}>
                <div className={classes.assetNum}>
                  <span className={classes.num}>32 </span> Assets
                </div>
                <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                <div className={classes.wish}>
                   <span className={classes.num}> 100 </span>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                  								
                </div>
              </div>
            </div>                
          </div>
        </div>
        
      
			</div>

      <div className={paging.paginationWrap}>
				<ul>
          <li><Link href="#" ><a className={paging.prev + ' ' + paging.disabled}>Prev</a></Link></li>
					<li><Link href="#" ><a>1</a></Link></li>
          <li className={paging.active}><span>...</span></li>
          <li><Link href="#" ><a>10</a></Link></li>
          <li><Link href="#" ><a>11</a></Link></li>
          <li className={paging.active}><Link href="#" ><a>12</a></Link></li>
          <li><Link href="#" ><a>16</a></Link></li>
          <li className={paging.active}><span>...</span></li>
					<li><Link href="#" ><a>26</a></Link></li>
					<li><Link href="#" ><a className={paging.next}>Next</a></Link></li>
				</ul>
			</div>

		</>
  );
}

