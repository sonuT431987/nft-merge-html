import React from 'react';
import Assets010 from './assets';
import Collections010 from './collections';
import Exhibitions010 from './exhibitions';
// import classes from './Thumbs010.module.scss'

const Thumbs010 = () => {
  return (
	<>
		<div className="tab-content">
			<div className="tab-pane fade " id="collectionWrap">
				<Collections010/>
			</div>
			<div className="tab-pane fade " id="exhibitionWrap">
				<Exhibitions010 />
			</div>
			<div className="tab-pane fade show active" id="assetsWrap">
				<Assets010 />
			</div>
		</div>
	
	</>
  );
}

export default Thumbs010;
