import React from 'react';
import Assets008 from './assets';
import Collections008 from './collections';
import Exhibitions008 from './exhibitions';
// import classes from './Thumbs008.module.scss'

const Thumbs008 = () => {
  return (
	<>
		<div className="tab-content">
			<div className="tab-pane fade show active" id="collectionWrap">
				<Collections008/>
			</div>
			<div className="tab-pane fade " id="exhibitionWrap">
				<Exhibitions008 />
			</div>
			<div className="tab-pane fade " id="assetsWrap">
				<Assets008 />
			</div>
		</div>
	
	</>
  );
}

export default Thumbs008;
