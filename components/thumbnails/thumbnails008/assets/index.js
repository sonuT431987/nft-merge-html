import React from "react";
import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-solid-svg-icons';

import paging from '@/components/thumbnails/thumbnails008/Thumbs008.module.scss';
import classes from './Assets008.module.scss';

import Thumb from '../../../../public/profile/profile008/images/thumbAssets.png';


export default function Assets008() {
      return (
		<>	
			<div className={classes.assetsAll}>

        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumb}
							width={240} height={323}							
							alt="Thumbs" />
						</a>							
						</Link>
					</div>
		      <div className={classes.thumbInfo}> 
            <div className={classes.thumbInfoInner}>           
              <div className={classes.thumbTitlewrap}>
                <h3>
                <Link href="/"><a>KUB Bittoon x The Mall Group</a></Link>
                </h3>							
              </div>
              <p className={classes.thumbDesc}>ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year</p>		
            </div>
          </div>
				</div>
        


			</div>

			{/* <div className={paging.paginationWrap}>
				<ul>
					<li className={paging.active}><Link href="#" ><a>1</a></Link></li>
					<li><Link href="#" ><a>2</a></Link></li>
					<li><Link href="#" ><a>3</a></Link></li>
				</ul>
			</div> */}

		</>
  );
}

