import React from "react";
import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import classes from './Exhibitions008.module.scss';

import Thumbs from '/public/profile/profile008/images/exhibitionthumb.png';

export default function Exhibitions008() {
 
  return (
		<>

    <div className={classes.exhibitionWraps}>
      <div className={classes.thumblist}>
        <div className={classes.thumbImg}>
          <Link className={classes.thumbsItem} href="#">
          <a>
            <Image
            src={Thumbs}
            width={1056} height={512}							
            alt="Thumbs" />
          </a>							
          </Link>
          {/* <div className={classes.wishLike}>
            <FontAwesomeIcon icon={faHeart} className="wishicon"/>
          </div> */}
        </div>
        <div className={classes.thumbInfo}>            
          <div className={classes.thumbTitlewrap}>
            <h3>
            <Link href="/"><a>Bitkub Chain M </a></Link>
            </h3>							
          </div>
          <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
          <div className={classes.descWishWrap}>
            <div className={classes.assetNum}>
              <span className={classes.num}>32 </span> Assets
            </div>
            <div className={classes.wish}>
              <FontAwesomeIcon icon={faHeart} className="wishicon"/>
              <span className={classes.num}> 100 </span>								
            </div>
          </div>								
        </div>
      </div>
    </div>


		</>
  );
}

