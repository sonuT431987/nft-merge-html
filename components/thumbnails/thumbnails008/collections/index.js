import React from "react";

import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import classes from './Collections008.module.scss';
import Thumbs from '/public/profile/profile008/images/thumb.png'; 


export default function Collections008() {

    return (
		<>

			<div className={classes.collectionWraps}>

				<div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumbs}
							width={512} height={512}							
							alt="Thumbs" />
						</a>							
						</Link>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
					</div>
					<div className={classes.thumbInfo}>            
						<div className={classes.thumbTitlewrap}>
							<h3>
							<Link href="/"><a>Bitkub Chain The NEXT Chapter </a></Link>
							</h3>							
						</div>
            <p className={classes.thumbDesc}>Bitkub Chain The NEXT Chapter </p>
						<div className={classes.descWishWrap}>
							<div className={classes.assetNum}>
                <span className={classes.num}>32 </span> Assets
              </div>
							<div className={classes.wish}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}> 100 </span>								
							</div>
						</div>								
					</div>
				</div>
        <div className={classes.thumblist}>
					<div className={classes.thumbImg}>
						<Link className={classes.thumbsItem} href="#">
						<a>
							<Image
							src={Thumbs}
							width={512} height={512}							
							alt="Thumbs" />
						</a>							
						</Link>
						<div className={classes.wishLike}>
							<FontAwesomeIcon icon={faHeart} className="wishicon"/>
						</div>
					</div>
					<div className={classes.thumbInfo}>            
						<div className={classes.thumbTitlewrap}>
							<h3>
							<Link href="/"><a>Bitkub Chain The NEXT Chapter </a></Link>
							</h3>							
						</div>
            <p className={classes.thumbDesc}>Bitkub Chain The NEXT Chapter </p>
						<div className={classes.descWishWrap}>
							<div className={classes.assetNum}>
                <span className={classes.num}>32 </span> Assets
              </div>
							<div className={classes.wish}>
                <FontAwesomeIcon icon={faHeart} className="wishicon"/>
								<span className={classes.num}> 100 </span>								
							</div>
						</div>								
					</div>
				</div>	



			</div>


			{/* <div className={paging.paginationWrap}>
				<ul>
					<li className={paging.active}><Link href="#" ><a>1</a></Link></li>
					<li><Link href="#" ><a>2</a></Link></li>
					<li><Link href="#" ><a>3</a></Link></li>
				</ul>
			</div> */}
		</>
  );
}

