import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import classes from './Collections009.module.scss';
import Thumbs from '/public/profile/profile009/images/thumb.png'; 


export default function Collections009() {
  const settings = {
    infinite: true,
    slidesToShow: 2,
    speed: 500,
    //rows: 2,
    slidesPerRow: 1,
    vertical: true,
    verticalSwiping: true,
    arrows:true,
  };

    return (
		<>

			<div className={classes.collectionWraps}>
        <Slider {...settings}>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumbs}
                width={784} height={364}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbTop}>
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>
                <div className={classes.thumbTopInfo}>
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Cinematic collection </a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                </div>
              </div>
              <div className={classes.thumbBottom}>
                <div className={classes.descWishWrap}>
                  <div className={classes.assetNum}>
                    <span className={classes.num}>32 </span> Assets
                  </div>
                  <div className={classes.wish}>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                    <span className={classes.num}> 100 </span>								
                  </div>
                </div>
              </div>          
              
                              
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumbs}
                width={784} height={364}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbTop}>
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>
                <div className={classes.thumbTopInfo}>
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Cinematic collection </a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                </div>
              </div>
              <div className={classes.thumbBottom}>
                <div className={classes.descWishWrap}>
                  <div className={classes.assetNum}>
                    <span className={classes.num}>32 </span> Assets
                  </div>
                  <div className={classes.wish}>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                    <span className={classes.num}> 100 </span>								
                  </div>
                </div>
              </div> 					
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumbs}
                width={784} height={364}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbTop}>
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>
                <div className={classes.thumbTopInfo}>
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Cinematic collection </a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                </div>
              </div>
              <div className={classes.thumbBottom}>
                <div className={classes.descWishWrap}>
                  <div className={classes.assetNum}>
                    <span className={classes.num}>32 </span> Assets
                  </div>
                  <div className={classes.wish}>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                    <span className={classes.num}> 100 </span>								
                  </div>
                </div>
              </div>          
              
                              
            </div>
          </div>
          <div className={classes.thumblist}>
            <div className={classes.thumbImg}>
              <Link className={classes.thumbsItem} href="#">
              <a>
                <Image
                src={Thumbs}
                width={784} height={364}							
                alt="Thumbs" />
              </a>							
              </Link>						
            </div>
            <div className={classes.thumbInfo}>  
              <div className={classes.thumbTop}>
                <div className={classes.wishLike}>
                  <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                </div>
                <div className={classes.thumbTopInfo}>
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Cinematic collection </a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus... </p>
                </div>
              </div>
              <div className={classes.thumbBottom}>
                <div className={classes.descWishWrap}>
                  <div className={classes.assetNum}>
                    <span className={classes.num}>32 </span> Assets
                  </div>
                  <div className={classes.wish}>
                    <FontAwesomeIcon icon={faHeart} className="wishicon"/>
                    <span className={classes.num}> 100 </span>								
                  </div>
                </div>
              </div> 					
            </div>
          </div>
        </Slider>
			</div>

		</>
  );
}

