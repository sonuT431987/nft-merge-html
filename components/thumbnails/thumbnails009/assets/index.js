import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";



import classes from './Assets009.module.scss';

import Thumb from '../../../../public/profile/profile009/images/thumbAssets.png';


export default function Assets009() {


	const settings = {
    infinite: true,
    slidesToShow: 3,
    speed: 500,
    //rows: 2,
    slidesPerRow: 4,
    vertical: true,
    verticalSwiping: true,
    arrows:true,
		responsive: [
		{
			breakpoint: 1199,
			settings: {
        slidesPerRow: 3,
        slidesToShow: 2,
      }
    },
    {
			breakpoint: 550,
			settings: {
        slidesPerRow: 2,
        slidesToShow: 2,
      }
    } 
		]
  };
  return (
		<>	
			<div className={classes.assetsAll}>
				<Slider {...settings}>
					<div className={classes.thumblist} style={{background:"url({Thumb})"}}>
						<div className={classes.thumbImg} >
							<Link className={classes.thumbsItem} href="#">
							{/* <a style={{background:'url("nft-merge-html/public/profile/profile009/images/thumbAssets.png")'}}> */}
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
					<div className={classes.thumblist}>
						<div className={classes.thumbImg}>
							<Link className={classes.thumbsItem} href="#">
							<a>
								<Image
								src={Thumb}
								width={240} height={323}							
								alt="Thumbs" />
							</a>							
							</Link>
						</div>
						<div className={classes.thumbInfo}> 
							<div className={classes.thumbInfoInner}>           
								<div className={classes.thumbTitlewrap}>
									<h3>
									<Link href="/"><a>Street Portrait Photography #7</a></Link>
									</h3>							
								</div>
								<p className={classes.thumbDesc}>Description ........</p>		
							</div>
						</div>
					</div>
				</Slider>
   
        

			</div>


		</>
  );
}

