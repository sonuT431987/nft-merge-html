import React from 'react';
import Assets009 from './assets';
import Collections009 from './collections';
import Exhibitions009 from './exhibitions';
// import classes from './Thumbs009.module.scss'

const Thumbs009 = () => {
  return (
	<>
		<div className="tab-content">
			<div className="tab-pane fade " id="collectionWrap">
				<Collections009/>
			</div>
			<div className="tab-pane fade " id="exhibitionWrap">
				<Exhibitions009 />
			</div>
			<div className="tab-pane fade show active" id="assetsWrap">
				<Assets009 />
			</div>
		</div>
	
	</>
  );
}

export default Thumbs009;
