import React, { useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";
import Button from 'public/account/Buttons.module.scss'
import classes from './Collection005.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Thumbs from '/public/collections/collections005/images/thumblist.png';
import Thumbs2 from '/public/collections/collections005/images/thumblist2.png';
import Thumbs3 from '/public/collections/collections005/images/thumblist3.png';
import Thumbs4 from '/public/collections/collections005/images/thumblist4.png';
import Thumbs5 from '/public/collections/collections005/images/thumblist5.png';
import Thumbs6 from '/public/collections/collections005/images/thumblist6.png';
import Thumbs7 from '/public/collections/collections005/images/thumblist7.png';
import Thumbs8 from '/public/collections/collections005/images/thumblist8.png';
import Thumbs9 from '/public/collections/collections005/images/thumblist9.png';
import Thumbs10 from '/public/collections/collections005/images/thumblist10.png';
import Thumbs11 from '/public/collections/collections005/images/thumblist11.png';
import Profile from '/public/collections/collections005/images/profile.png';
import downArrow from '/public/collections/collections005/images/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faPen, faWallet } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);

export default function Collection005() {
  useEffect(() => {
      document.querySelector("body").classList.add("collectionFive");
  });
  const settings = {
    infinite: true,
    slidesToShow: 2,
    speed: 500,
    rows: 2,
    slidesPerRow: 3,
    //vertical: true,
    //verticalSwiping: true,
    arrows:true,
   

		responsive: [
		{
			breakpoint: 1024,
			settings: {
        slidesPerRow: 1,
        slidesToShow: 4,
      }
    },
    {
			breakpoint: 550,
			settings: {
        slidesPerRow: 1,
        slidesToShow: 2,
      }
    } 
		]
  };

	return (
		<>  
			<div className={classes.collectionWrap}>
				<div className={`container` + ' ' + classes.container}>
					<div className={classes.collectionTop}>
						<div className={classes.headSocial}>
							<span className={classes.socialLabel}>Share:</span> 
							<Link  href="#">
								<a>
									<FontAwesomeIcon icon={faTwitter} size="lg" /> 
								</a>
							</Link>
							<Link  href="#">
								<a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
							</Link>
							<Link  href="#">
								<a><FontAwesomeIcon icon={faLink} size="lg" /></a>
							</Link>
						</div>
					</div>

          <div className="row">
            <div className="col-md-8 mx-auto">
              <div className="row">
                <div className="col-md-4">
                  <div className={classes.userImg}>
                    <Image className={classes.profileImg} src={Profile} width={321} height={318} alt="Profile Image" />
                  </div>
                </div>
                <div className="col-md-8">
                  <div className={classes.headWrap}>
                    <h1>Leeds united Collection</h1>
                    <h6>(50 Assets)</h6>
                  </div>
                  <p className={classes.userDesc}>
                  Leeds United Football Club is an English professional football club based in the city of Leeds, West Yorkshire. The club was formed in 1919 following the disbanding of Leeds City by the Football League and took over their Elland Road stadium
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className={classes.collectionAll + ' ' + classes.slickSlider}>
            <Slider {...settings }>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs2}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs3}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs4}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs5}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs6}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs7}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs8}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs9}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs10}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs11}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs2}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs3}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs4}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs5}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs6}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs7}
                      height={324} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>
                <div className={classes.thumbInfo}>
                  <h5 className={classes.thumbTitle}><Link href="#"><a>NFT Name</a></Link></h5> 
                  <p className={classes.thumbDesc}>Discription ...</p> 
                </div>                  
              </article>
            </Slider>
          </div>

				</div>
			</div>
	
      <div className={classes.fixIcons}>
        <Link href="#">
				<a className={`btn ${classes.btnIcons + ' ' +  Button.btnOutlinePrimary}`}><FontAwesomeIcon icon={faWallet} size="lg" /></a></Link>
			</div>
			<div className={classes.footer}>
				<div className={`container` + ' ' + classes.container}>
					<div className={classes.profileLike}>
						<div className={classes.profileWrap}>
							<div className={classes.userLogged}>
                <div className={`mb-0` + '' + classes.userImg} style={{marginRight:12}}>
                  <Image className={classes.profileImg} src={Profile} width={60} height={60} alt="Profile Image" />
                </div>
								<div className={classes.userInfo}>
									<h4>@Soundmint</h4>
									<div className={classes.viewLink}>
										<Link href="#">
										<a>view profile <FontAwesomeIcon icon={faArrowRight} size="lg" /></a>
										</Link>
									</div>
								</div>
							</div>
							<div className={classes.likesCount}>
								<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
								99K
							</div>
						</div>
					</div>			
				</div>
        <div className={classes.copyrightWrap}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.copyright}>
              Copyright 2022 ©username. All rights reserved. | Powered by  <span className={classes.copyrightLogo}><Image src={downArrow} alt='Down Logo' width={26} height={22}  /> </span> NFTIFY
            </div>
          </div>
        </div>
      </div>
		</>
    );
}

