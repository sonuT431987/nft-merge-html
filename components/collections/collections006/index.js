import React, { useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";
import Button from 'public/account/Buttons.module.scss'
import classes from './Collection006.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Thumbs from 'public/collections/collections006/images/thumblist.png';
import Profiletop from '/public/collections/collections006/images/profiletop.png';
import Profile from '/public/collections/collections006/images/profile.png';
import downArrow from '/public/collections/collections006/images/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faPen, faWallet } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    />
  );
}

export default function Collection006() {
  useEffect(() => {
      document.querySelector("body").classList.add("collectionSix");
  });
  const settings = {
    infinite: true,
    slidesToShow: 2,
    speed: 500,
    //rows: 1,
    slidesPerRow: 3,
    vertical: true,
    verticalSwiping: true,
    arrows:true,
    //nextArrow: <SampleNextArrow />,
    //prevArrow: <SamplePrevArrow />,

		responsive: [
		{
			breakpoint: 991,
			settings: {
        slidesPerRow: 2,
        slidesToShow: 2,
      }
    },
    {
			breakpoint: 767,
			settings: {
        slidesPerRow: 2,
        slidesToShow: 2,
      }
    } 
		]
  };

	return (
		<>  
			<div className={classes.collectionWrap}>
        <div className={classes.collectionHeader}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.collectionTop}>
              <div className={classes.headSocial}>
                <span className={classes.socialLabel}>Share:</span> 
                <Link  href="#">
                  <a>
                    <FontAwesomeIcon icon={faTwitter} size="lg" /> 
                  </a>
                </Link>
                <Link  href="#">
                  <a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
                </Link>
                <Link  href="#">
                  <a><FontAwesomeIcon icon={faLink} size="lg" /></a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.collectionContents}>
          <div className={`container` + ' ' + classes.container}>
            <div className="row">
              <div className="col-md-8">
                <div className={classes.collectionAll + ' ' + classes.slickSlider}>
                  <Slider {...settings }>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    <article className={classes.thumbList}>
                      <div className={classes.thumbImg}>
                        <Link className={classes.thumbsItem} href="#">
                          <a>
                          <Image
                            src={Thumbs}
                            height={241} width={240} 
                            alt="Thumbs"
                          />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.thumbInfo}>
                        <h5 className={classes.thumbTitle}><Link href="#"><a>3D Soft Curves</a></Link></h5> 
                        <p className={classes.thumbDesc}>Current Bid</p> 
                      </div>                  
                    </article>
                    
                  </Slider>
                </div>
              </div>

              <div className="col-md-4">
                <div className={classes.sidebarBanner}>
                  <div className={classes.headWrap}>
                    <h1>JOSHUA KIMMI</h1>
                    <h6>(50 Assets)</h6>
                  </div>
                  <div className={classes.userImg}>
                    <Image className={classes.profileImg} src={Profiletop} width={512} height={512} alt="Profile Image" />
                  </div>
                  <p className={classes.userDesc}>
                  Leeds United Football Club is an English professional football club based in the city of Leeds, West Yorkshire. The club was formed in 1919 following the disbanding of Leeds City by the Football League and took over their Elland Road stadium
                  </p>
                </div>
              </div>
            </div>

          </div>
        </div>
			</div>
	
      <div className={classes.fixIcons}>
        <Link href="#">
				<a className={`btn ${classes.btnIcons + ' ' +  Button.btnOutlinePrimary}`}><FontAwesomeIcon icon={faWallet} size="lg" /></a></Link>
			</div>
			<div className={classes.collectionFooter}>
				<div className={`container` + ' ' + classes.container}>
					<div className={classes.profileLike}>
						<div className={classes.profileWrap}>
							<div className={classes.userLogged}>
                <div className={`mb-0` + '' + classes.userImg} style={{marginRight:12}}>
                  <Image className={classes.profileImg} src={Profile} width={60} height={60} alt="Profile Image" />
                </div>
								<div className={classes.userInfo}>
									<h4>@WonderPal</h4>
									<div className={classes.viewLink}>
										<Link href="#">
										<a>view profile <FontAwesomeIcon icon={faArrowRight} size="lg" /></a>
										</Link>
									</div>
								</div>
							</div>
							<div className={classes.likesCount}>
								<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
								99K
							</div>
						</div>
					</div>			
				</div>
        <div className={classes.copyrightWrap}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.copyright}>
              Copyright 2022 ©username. All rights reserved. | Powered by  <span className={classes.copyrightLogo}><Image src={downArrow} alt='Down Logo' width={26} height={22}  /> </span> NFTIFY
            </div>
          </div>
        </div>
      </div>
		</>
    );
}

