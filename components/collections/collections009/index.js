import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Collection009.module.scss';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";


import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faChevronDown, faChevronUp, faLink, faWallet } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faHeart, faTwitter, faFacebook, faLink, faWallet, faChevronDown, faChevronUp);


import Thumbs from '../../../public/dummyassets/thumbs.png';
import Profile from '../../../public/collections/collections009/images/profile.png';
import downArrow from '../../../public/dummyassets/down-arrow.png';



export default function Collection008() {
  useEffect(() => {``
      document.querySelector("body").classList.add("collection009");
  });
  const settings = {
    className: "center",
    infinite: false,
    centerPadding: "16px",
    slidesToShow: 4,
    speed: 500, 
    rows: 2,
    slidesPerRow: 1,
    arrows:true,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
			slidesToShow: 2,
			variableWidth: false,
		}
    	} 
	]
  };

	return (
		<>  
			<div className={classes.collectionWrap}>
				<div className="container">
					<div className="row">
						<div className="col-md-4">
							<div className={classes.bannerImg}>
								<Image
									src={Profile}
									layout="fill" className={classes.customImg}
									alt="Thumbs"
								/>
							</div>
						</div>
						<div className="col-md-8">
							<div className={classes.collectionTop}>
								<div className={classes.headSocial}>
									<span className={classes.socialLabel}>Share:</span> 
									<Link  href="#">
										<a>
											<FontAwesomeIcon icon={faTwitter} size="lg" /> 
										</a>
									</Link>
									<Link  href="#">
										<a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
									</Link>
									<Link  href="#">
										<a><FontAwesomeIcon icon={faLink} size="lg" /></a>
									</Link>
								</div>
							</div>
							<div className={classes.headWrap}>
								<h1>Cinematic collection</h1>
								<h6>(12 Assets)</h6>
							</div>
							<div className={classes.userDesc}>
								Leeds United Football Club is an English professional football club based in the city of Leeds, West Yorkshire. The club was formed in 1919 following the disbanding of Leeds City by the Football League and took over their Elland Road stadium 
							</div>
							<div className={classes.assetsAll}>
							<Slider {...settings }>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								<article className={classes.thumbList}>
									<div className={classes.thumbImg}>
									<Link className={classes.thumbsItem} href="#">
										<a>
										<Image
										src={Thumbs}
										height={324} width={240} 
										alt="Thumbs"
										/>
										</a>
									</Link>
									</div>
									<div className={classes.thumbInfo}>
									<h5 className={classes.thumbTitle}><Link href="#"><a>Street Portrait<br/>Photography #7</a></Link></h5> 
									<p className={classes.thumbDesc}>Discription ...</p> 
									</div>                  
								</article>
								</Slider>
							</div>
							<div className={classes.footer}>
								<div className={classes.profileWrap}>
									<div className={classes.userLogged}>
										<div className={classes.userImg}>
											<Image className={classes.profileImg} src={Profile} width={48} height={48} alt="Profile Image" />
										</div>
										<div className={classes.userInfo}>
											<h4>@Deekaykwon</h4>
											<div className={classes.viewLink}><Link href="/"><a>view profile <FontAwesomeIcon icon={faArrowRight} size="1x" /></a></Link></div>
										</div>
									</div>
									<div className={classes.likesCount}>
										<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
										99K
									</div>
								</div>
								<div className="container">
									<div className={classes.copyright}>Copyright 2022 ©username. All rights reserved. | Powered by <span className={classes.copyrightLogo}><Image src={downArrow} alt='Down Logo' width={25} height={21} /></span> NFTIFY</div>
								</div>
							</div>
							<div className={classes.fixIcons}>
								<span><button type="button" className={`btn w-100 ${classes.btnIcons}`}><FontAwesomeIcon icon={faWallet} size="lg" /></button></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</>
    );
}

