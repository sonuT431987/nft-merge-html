import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Collection008.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faChevronDown, faChevronUp, faLink, faWallet } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faHeart, faTwitter, faFacebook, faLink, faWallet, faChevronDown, faChevronUp);


import Thumbs from '../../../public/dummyassets/thumbs.png';
import Profile from '../../../public/collections/collections008/images/profile.png';
import downArrow from '../../../public/dummyassets/down-arrow.png';



export default function Collection008() {
  useEffect(() => {
      document.querySelector("body").classList.add("collection008");
  });
  

	return (
		<>  
			<div className={classes.collectionWrap}>
				<div className="container">
					<div className="row">
						<div className="col-md-8 ">
							<div className={classes.collectionTop}>
								<div className={classes.headSocial}>
									<span className={classes.socialLabel}>Share:</span> 
									<Link  href="#">
										<a>
											<FontAwesomeIcon icon={faTwitter} size="lg" /> 
										</a>
									</Link>
									<Link  href="#">
										<a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
									</Link>
									<Link  href="#">
										<a><FontAwesomeIcon icon={faLink} size="lg" /></a>
									</Link>
								</div>
							</div>
							<div className={classes.headWrap}>
								<h1>THUNDER GRAFFITI 🌪⚡️</h1>
							</div>
							<div className={classes.userDesc}>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and 
							</div>
							<div className={classes.btnWrap}><button type="button" className={`btn ${classes.prevBtn}`}><FontAwesomeIcon icon={faChevronUp} size="lg" /></button> </div>
							<div className={classes.assetsAll}>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
								<article className={classes.thumbsWrapAll}>
									<div className={classes.front}>
										<Link className={classes.thumbsItem} href="/">
											<a>
											<Image
												src={Thumbs}
												layout="fill"
												alt="Thumbs"
											/>
											</a>
										</Link>
									</div>
									<div className={classes.back}>
										<div className={classes.thumbDescWrap}>
											<h3>KUB Bittoon x The Mall Group</h3>
											<p className={classes.thumbDesc}>
											ยินดีด้วยคุณได้รับ KUB Bittoon Collectible NFT - The Great Happy New Year
											</p>
										</div>
									</div>
								</article>
							</div>
							<div className={classes.btnWrap}><button type="button" className={classes.nextBtn}><FontAwesomeIcon icon={faChevronDown} size="lg" /></button> </div>
							<div className={classes.profileWrap}>
								<div className={classes.userLogged}>
									<div className={classes.userImg}>
										<Image className={classes.profileImg} src={Profile} width={48} height={48} alt="Profile Image" />
									</div>
									<div className={classes.userInfo}>
										<h4>@Deekaykwon</h4>
										<div className={classes.viewLink}><Link href="/"><a>view profile <FontAwesomeIcon icon={faArrowRight} size="1x" /></a></Link></div>
									</div>
								</div>
								<div className={classes.likesCount}>
									<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
									99K
								</div>
							</div>
														
							<div className={classes.footer}>
								<div className="container">
									<div className={classes.copyright}>Copyright 2022 ©username. All rights reserved. | Powered by <span className={classes.copyrightLogo}><Image src={downArrow} alt='Down Logo' width={25} height={21} /></span> NFTIFY</div>
								</div>
							</div>
						</div>
						<div className="col-md-4">
							<div className={classes.bannerImg}>
								<Image
									src={Profile}
									layout="fill" className={classes.customImg}
									alt="Thumbs"
								/>
								<div className={classes.fixIcons}>
								<span><button type="button" className={`btn w-100 ${classes.btnIcons}`}><FontAwesomeIcon icon={faWallet} size="lg" /></button></span>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</>
    );
}

