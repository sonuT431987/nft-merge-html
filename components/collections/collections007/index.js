import React, { useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";
import Button from 'public/account/Buttons.module.scss'
import classes from './Collection007.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Thumbs from 'public/collections/collections007/images/thumblist.png';
import Profiletop from '/public/collections/collections007/images/profiletop.png';
import Profile from '/public/collections/collections007/images/profile.png';
import downArrow from '/public/collections/collections007/images/down-arrow.png';

import { faShareNodes, faArrowRight, faLink,faPen, faWallet } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "#DB2A15", color:"#fff", }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "#DB2A15" }}
      onClick={onClick}
    />
  );
}

export default function Collection007() {
  useEffect(() => {
      document.querySelector("body").classList.add("collectionSeven");
  });
  const settings = {
    infinite: true,
    slidesToShow: 5,
    speed: 500,
    //rows: 1,
    //slidesPerRow: 3,
    //vertical: true,
    //verticalSwiping: true,
    arrows:true,
    //nextArrow: <SampleNextArrow />,
    //prevArrow: <SamplePrevArrow />,

		responsive: [
		{
			breakpoint: 1199,
			settings: {
        slidesToShow: 3,
      }
    },
    {
			breakpoint: 767,
			settings: {
        slidesToShow: 2,
      }
    } 
		]
  };

	return (
		<>  
			<div className={classes.collectionWrap}>
        <div className={classes.collectionHeader}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.collectionTop}>
              <div className={classes.headSocial}>
                <span className={classes.socialLabel}>Share:</span> 
                <Link  href="#">
                  <a>
                    <FontAwesomeIcon icon={faTwitter} size="lg" /> 
                  </a>
                </Link>
                <Link  href="#">
                  <a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
                </Link>
                <Link  href="#">
                  <a><FontAwesomeIcon icon={faLink} size="lg" /></a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.collectionContents}>
          <div className={`container` + ' ' + classes.container}>
            <div className={`row align-items-center` + ' ' + classes.userSectionWrap}>
              <div className="col-md-6">
                <div className={classes.userImg}>
                  <Image className={classes.profileImg} src={Profiletop} width={641} height={512} alt="Profile Image" />
                </div>
              </div>
              <div className="col-md-5">
                <div className={classes.headWrap}>
                    <h1>Flower Boy Edition</h1>
                    <h6>(99 Assets)</h6>
                </div>                  
                <p className={classes.userDesc}>
                Created by SoundMint <br/>
                KLOUD is the artistical embodiment of limitless creativity in anonymity. With this NFT drop, the collector enters the KLOUD, owning a unique visual & musical art piece derived from the generative algorithm that is KLOUD x HOOKER x COMPUTER. Holding one of these NFTs grants future access to KLOUD metaverse events.
                </p>
              </div>
            </div>

            <div className={classes.collectionAll + ' ' + classes.slickSlider}>
              <Slider {...settings }>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
                <article className={classes.thumbList}>
                  <div className={classes.thumbImg}>
                    <Link className={classes.thumbsItem} href="#">
                      <a>
                      <Image
                        src={Thumbs}
                        height={241} width={240} 
                        alt="Thumbs"
                      />
                      </a>
                    </Link>
                  </div>                
                </article>
              </Slider>
            </div>

          </div>
        </div>
        <div className={classes.fixIcons}>
        {/* <Link href="#">
				<a className={`btn ${classes.btnIcons + ' ' +  Button.btnOutlinePrimary}`}><FontAwesomeIcon icon={faPen} size="lg" />
        </a></Link> */}
        <Link href="#">
				<a className={`btn ${classes.btnIcons + ' ' +  Button.btnOutlinePrimary}`}><FontAwesomeIcon icon={faWallet} size="lg" />
        </a></Link>
			</div>
			<div className={classes.collectionFooter}>
				<div className={`container` + ' ' + classes.container}>
					<div className={classes.profileLike}>
						<div className={classes.profileWrap}>
							<div className={classes.userLogged}>
                <div className={`mb-0` + '' + classes.userImg} style={{marginRight:12}}>
                  <Image className={classes.profileImg} src={Profile} width={60} height={60} alt="Profile Image" />
                </div>
								<div className={classes.userInfo}>
									<h4>@Flowerboy</h4>
									<div className={classes.viewLink}>
										<Link href="#">
										<a>view profile <FontAwesomeIcon icon={faArrowRight} size="lg" /></a>
										</Link>
									</div>
								</div>
							</div>
							<div className={classes.likesCount}>
								<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
								99K
							</div>
						</div>
					</div>			
				</div>
        <div className={classes.copyrightWrap}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.copyright}>
              Copyright 2022 ©username. All rights reserved. | Powered by  <span className={classes.copyrightLogo}><Image src={downArrow} alt='Down Logo' width={26} height={22}  /> </span> NFTIFY
            </div>
          </div>
        </div>
      </div>

			</div>
	
      
		</>
    );
}

