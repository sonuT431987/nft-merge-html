import React, { useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
import Link from "next/link";
import Button from 'public/account/Buttons.module.scss'
import classes from './Collection010.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Thumbs from 'public/collections/collections010/images/thumblist.png';
import Profiletop from '/public/collections/collections010/images/profiletop.png';
import Profile from '/public/collections/collections010/images/profile.png';
import downArrow from '/public/collections/collections010/images/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faPen } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);



export default function Collection010() {
  useEffect(() => {
      document.querySelector("body").classList.add("collectionTen");
  });

	return (
		<>  
			<div className={classes.collectionWrap}>
        <div className={classes.collectionHeader}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.collectionTop}>
              <div className={classes.headSocial}>
                <span className={classes.socialLabel}>Share:</span> 
                <Link  href="#">
                  <a>
                    <FontAwesomeIcon icon={faTwitter} size="lg" /> 
                  </a>
                </Link>
                <Link  href="#">
                  <a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
                </Link>
                <Link  href="#">
                  <a><FontAwesomeIcon icon={faLink} size="lg" /></a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.collectionContents}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.userSectionWrap}>
              <div className={classes.userImg}>
                <Image className={classes.profileImg} src={Profiletop} width={1600} height={311} alt="Profile Image" />
              </div>    
              <div className={classes.headWrap}>
                <h1>One Piece Collection</h1>
              </div>                  
              <p className={classes.userDesc}>
              Leeds United Football Club is an English professional football club based in the city of Leeds, West Yorkshire. The club was formed in 1919 following the <br/> disbanding of Leeds City by the Football League and took over their Elland Road stadium ...
              </p>
            </div>

            <div className={classes.collectionAll}>

              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>
              <article className={classes.thumbList}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                    <Image
                      src={Thumbs}
                      height={344} width={240} 
                      alt="Thumbs"
                    />
                    </a>
                  </Link>
                </div>                
              </article>

            </div>

          </div>
        </div>
        <div className={classes.fixIcons}>
        <Link href="#">
				<a className={`btn ${classes.btnIcons + ' ' +  Button.btnOutlinePrimary}`}><FontAwesomeIcon icon={faPen} size="lg" /></a></Link>
			</div>
			<div className={classes.collectionFooter}>
				<div className={`container` + ' ' + classes.container}>
					<div className={classes.profileLike}>
						<div className={classes.profileWrap}>
							<div className={classes.userLogged}>
                <div className={`mb-0` + '' + classes.userImg} style={{marginRight:12}}>
                  <Image className={classes.profileImg} src={Profile} width={50} height={50} alt="Profile Image" />
                </div>
								<div className={classes.userInfo}>
									<h4>@Flowerboy</h4>
									<div className={classes.viewLink}>
										<Link href="#">
										<a>view profile <FontAwesomeIcon icon={faArrowRight} size="lg" /></a>
										</Link>
									</div>
								</div>
							</div>
							<div className={classes.likesCount}>
								<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
								99K
							</div>
						</div>
					</div>			
				</div>
        <div className={classes.copyrightWrap}>
          <div className={`container` + ' ' + classes.container}>
            <div className={classes.copyright}>
              Copyright 2022 ©username. All rights reserved. | Powered by  <span className={classes.copyrightLogo}><Image src={downArrow} alt='Down Logo' width={26} height={22}  /> </span> NFTIFY
            </div>
          </div>
        </div>
      </div>

			</div>
	
      
		</>
    );
}

