import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Collection003.module.scss';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


import Thumbs from '../../../public/dummyassets/thumbs.png';
import Profile from '../../../public/dummyassets/profile.png';
import downArrow from '../../../public/dummyassets/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faWallet, faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faWallet, faAngleLeft, faAngleRight);



export default function Collection003() {
    useEffect(() => {
        document.querySelector("body").classList.add("collection003");
    });
  return (
    <>
        <div className={classes.collectionWrap}>
            <div className='container'>
                <div className={classes.collectionTop}>
                    <div className={classes.headWrap}>
                        <h1>Boy in the city</h1>
                        <h6>(100 Assets)</h6>
                    </div>
                    <div className={classes.headSocial}>Share: <Link href="/">
							<a>
								<FontAwesomeIcon icon={faTwitter} size="lg" /> 
							</a>
						</Link>
						<Link href="/">
							<a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
						</Link>
						<Link href="/">
							<a><FontAwesomeIcon icon={faLink} size="lg" /></a>
						</Link></div>
                </div>
                <div className={classes.userDesc}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie maecenas dui condimentum vitae, integer leo, eget tortor. Faucibus...
                </div>
            </div>
        </div>
        <div className='container'>
            <div className={classes.assetsAll}>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>

                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
                    <article className={classes.thumbsWrapAll}>
                        <div className={classes.front}>
                            <Link className={classes.thumbsItem} href="/">
                                <a>
                                <Image
                                    src={Thumbs}
                                    height={188}
                                    width={188}
                                    alt="Thumbs"
                                />
                                </a>
                            </Link>
                        </div>
                        <div className={classes.back}>
                            <h3>NFT<br/>Name</h3>
                        </div>
                    </article>
            </div>
                <div className={classes.paginationWrap}>
                    <li className={classes.arrowIcons}><Link href="/"><a><FontAwesomeIcon icon={faAngleLeft} /></a></Link></li>
                    <li className={classes.arrowIcons}><Link href="/"><a><FontAwesomeIcon icon={faAngleRight} /></a></Link></li>
                </div>
                <div className={classes.profileLike}>
				<div className="container">
					<div className={classes.profileWrap}>
						<div className={classes.userLogged}>
							<div className={classes.userImg}>
								<Image className={classes.profileImg} src={Profile} width={48} height={48} alt="Profile Image" />
							</div>
							<div className={classes.userInfo}>
								<h4>@Deekaykwon</h4>
								<div className={classes.viewLink}><Link href="/"><a>view profile <FontAwesomeIcon icon={faArrowRight} size="1x" /></a></Link></div>
							</div>
						</div>
						<div className={classes.likesCount}>
							<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
							99K
						</div>
					</div>
				</div>
			</div>
			<div className={classes.fixIcons}>
				<span><button type="button" className={`btn w-100 ${classes.btnIcons}`}><FontAwesomeIcon icon={faWallet} size="lg" /></button></span>
			</div>
			
			<div className={classes.footer}>
				<div className="container">
					<div className={classes.copyright}>Copyright 2022 ©username. All rights reserved. | Powered by <span className={classes.footerImg}><Image src={downArrow} alt='Down Logo' width={25} height={21} /></span> NFTIFY</div>
				</div>
			</div>
        </div>
    </>
  );
}

