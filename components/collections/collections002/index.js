import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './Collection002.module.scss';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Thumbs from '../../../public/dummyassets/thumbs.png';
import Profile from '../../../public/dummyassets/profile.png';
import downArrow from '../../../public/dummyassets/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faPen, faWallet } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);


export default function Collection002() {
    useEffect(() => {
        document.querySelector("body").classList.add("collection002");
    });
      return (
		<>
			<div className={classes.collectionWrap}>
				<div className='container'>
					<div className={classes.collectionTop}>
						<div className={classes.headWrap}>
							<h1>Boy in the city</h1>
							<h6>(100 Assets)</h6>
						</div>
						<div className={classes.headSocial}>Share: <Link href="/">
							<a>
								<FontAwesomeIcon icon={faTwitter} size="lg" /> 
							</a>
						</Link>
						<Link href="/">
							<a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
						</Link>
						<Link href="/">
							<a><FontAwesomeIcon icon={faLink} size="lg" /></a>
						</Link></div>
					</div>
				</div>
			</div>
			<div className='container'>
				<div className={classes.userDesc}>
					Lorem Ipsum is simply dummy text of the printing and type setting industry.
				</div>
				<div className={classes.assetsAll}>
					<article className={classes.thumbsWrap}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					</div>
					<div className={classes.thumbInfo}>
						<h3 className={classes.thumbTitle}>Lonely Deekay</h3>
						<p className={classes.thumbDesc}>
						Lorem ipsum dolor sit amet, consectetur ist at...
						</p>
					</div>
					</article>
					<article className={classes.thumbsWrap}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					</div>
					<div className={classes.thumbInfo}>
						<h3 className={classes.thumbTitle}>Client teach me</h3>
						<p className={classes.thumbDesc}>
						Lorem ipsum dolor sit amet, consectetur ist at...
						</p>
					</div>
					</article>
					<article className={classes.thumbsWrap}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					</div>
					<div className={classes.thumbInfo}>
						<h3 className={classes.thumbTitle}>Client teach me</h3>
						<p className={classes.thumbDesc}>
						Lorem ipsum dolor sit amet, consectetur ist at...
						</p>
					</div>
					</article>
					<article className={classes.thumbsWrap}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					</div>
					<div className={classes.thumbInfo}>
						<h3 className={classes.thumbTitle}>Client teach me</h3>
						<p className={classes.thumbDesc}>
						Lorem ipsum dolor sit amet, consectetur ist at...
						</p>
					</div>
					</article>
					<article className={classes.thumbsWrap}>
					<div className={classes.thumbImg}>
					<Link className={classes.thumbsItem} href="/">
						<a>
						<Image
							src={Thumbs}
							height={512}
							width={512}
							alt="Thumbs"
						/>
						</a>
					</Link>
					</div>
					<div className={classes.thumbInfo}>
						<h3 className={classes.thumbTitle}>Titles</h3>
						<p className={classes.thumbDesc}>Thumbs Descriptions</p>
					</div>
					</article>
				</div>
				<div className={classes.paginationWrap}>
					<ul>
						<li className={classes.active}><Link href="/" ><a>1</a></Link></li>
						<li><Link href="/" ><a>...</a></Link></li>
						<li><Link href="/" ><a>10</a></Link></li>
						<li><Link href="/" ><a>11</a></Link></li>
						<li><Link href="/" ><a>...</a></Link></li>
						<li><Link href="/" ><a>26</a></Link></li>
						<li><Link href="/" ><a>Next</a></Link></li>
					</ul>
				</div>
			<div className={classes.profileLike}>
				<div className="container">
					<div className={classes.profileWrap}>
						<div className={classes.userLogged}>
							<div className={classes.userImg}>
								<Image className={classes.profileImg} src={Profile} width={48} height={48} alt="Profile Image" />
							</div>
							<div className={classes.userInfo}>
								<h4>@Deekaykwon</h4>
								<div className={classes.viewLink}><Link href="/"><a>view profile <FontAwesomeIcon icon={faArrowRight} size="1x" /></a></Link></div>
							</div>
						</div>
						<div className={classes.likesCount}>
							<span className="d-block"><FontAwesomeIcon icon={faHeart} size="lg" /></span>
							99K
						</div>
					</div>
				</div>
			</div>
			<div className={classes.fixIcons}>
				<span><button type="button" className={`btn w-100 ${classes.btnIcons}`}><FontAwesomeIcon icon={faWallet} size="lg" /></button></span>
			</div>
			
			<div className={classes.footer}>
				<div className="container">
					<div className={classes.copyright}>Copyright 2022 ©username. All rights reserved. | Powered by <span className={classes.footerImg}><Image src={downArrow} alt='Down Logo' width={25} height={21} /></span> NFTIFY</div>
				</div>
			</div>
			</div>
		</>
  );
}

