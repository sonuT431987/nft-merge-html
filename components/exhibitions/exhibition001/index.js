import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './exhibition001.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import userImg from 'public/exhibitions/exhibition001/images/userImg.png';
import iframeImg from 'public/exhibitions/exhibition001/images/iframeImg.png';
import downArrow from 'public/exhibitions/exhibition001/images/down-arrow.png';


import { faShareNodes, faArrowRight, faLink, faPen } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);


export default function Exhibition001() {
    useEffect(() => {
        document.querySelector("body").classList.add("exhibitionOne");
    }); 
	

	return (
		<>
		<div className={classes.exhibitionWrap}>
      {/* <iframe scrolling="no" className={classes.iframeBody} width="100%" height="100%"
        src="https://tours.teedd360.com/nft-1/" name="imgbox" id="imgbox">
      </iframe> */}

      <div className={classes.exhibitionIframe + ' ' + classes.iframeBody}>
        <Image src={iframeImg} alt='User Image' width={1920} height={1080} className={classes.userImg} />
      </div>

      <div className={classes.exhibitionInner}>
        <div className={classes.modeListWrap}>
          <div className="container">
            <div className={classes.modeList}>
              <Link href="/exhibitions/exhibition001">
                <a className={classes.modeLink + ' ' + classes.active}>VR MODE</a>
              </Link>
              <Link href="/exhibitions/exhibition001/assets">
                <a className={classes.modeLink}>ASSETS</a>
              </Link> 
            </div>
          </div>
        </div>

        <div className={classes.exhibitionContent}>
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <h1 className={classes.title}>Leeds united </h1>
                <div className={classes.userLogged}>
                  <div className={classes.userImg}>
                    <Image src={userImg} alt='User Image' width={36} height={30} className={classes.userImg} />
                  </div>
                  <div className={classes.userInfo}>
                    <h4 className={classes.name}>Alexandre</h4>
                    <h3 className={classes.userName}>@username</h3>
                  </div>
                </div>
                <p className={classes.userDesc}>
                Leeds United Football Club is an English professional football club based in the city of Leeds, West Yorkshire. The club was formed in 1919 following the disbanding of Leeds City by the Football League and took over their Elland Road stadium
                </p>
                <Link  href="exhibition001/preview">
                <a className={`btn ` + ' ' + 'btn-default' + ' ' + classes.linkBtn}>ENTER WORLD</a>
                </Link>
              </div>
              <div className={`col-md-2` + ' ' + classes.socialCol}>
                <div className={classes.socialList}>
                  <span className={classes.socialLabel}>Share:</span> 
                  <Link  href="#">
                    <a>
                      <FontAwesomeIcon icon={faTwitter} size="lg" /> 
                    </a>
                  </Link>
                  <Link  href="#">
                    <a><FontAwesomeIcon icon={faFacebook} size="lg" /> </a>
                  </Link>
                  <Link  href="#">
                    <a><FontAwesomeIcon icon={faLink} size="lg" /></a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className={classes.footer}>
          <div className="container">
            <div className={classes.copyright}>Powered by  <Image src={downArrow} alt='Down Logo' width={36} height={30} className={classes.copyrightLogo} /> </div>
          </div>
        </div>
      </div>
      
		</div>
			
			

			
		</>
    );
}

