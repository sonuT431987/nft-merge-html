import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './exhibition-preview.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import userImg from 'public/exhibitions/exhibition001/images/userImg.png';
import downArrow from 'public/exhibitions/exhibition001/images/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faPen, faShareAlt, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";

import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);


export default function Exhibition001Preview() {
    useEffect(() => {
        document.querySelector("body").classList.add("exhibitionOne");
    });
	

	return (
		<>
  
		<div className={classes.exhibitionWrap}>
      <div className={`container-fluid` + ' ' + classes.headerPreview}>
        <h1 className={classes.Headertitle}>Room name </h1>
        <div className={classes.headerList}>
          <div className={classes.userLogged}>
            <div className={classes.userImg}>
              <Image src={userImg} alt='User Image' width={36} height={30} className={classes.userImg} />
            </div>
            <div className={classes.userInfo}>
              <h4 className={classes.name}>Alexandre</h4>
              <h3 className={classes.userName}>@username</h3>
            </div>
          </div>
          <div className={classes.socialHeader}>
            <Link href="#" >
              <a className={`btn btn-default` + ' ' + classes.btnLink}>
                <FontAwesomeIcon icon={faThumbsUp} size="lg" />
                like
              </a>
            </Link>
            <Link href="#">
              <a  className={`btn btn-default` + ' ' + classes.btnLink}>
                <FontAwesomeIcon icon={faShareAlt} size="lg" /> 
                Share
              </a>
            </Link>
          </div>
        </div>
      </div>
      <iframe scrolling="no" className={classes.iframeBody} width="100%" height="100%"
        src="https://tours.teedd360.com/the-sea-koh-samui/" name="imgbox" id="imgbox">
      </iframe>

      <div className={classes.exhibitionInner}>
      

        <div className={classes.exhibitionContent}>
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                
                                
              </div>
   
            </div>
          </div>
        </div>

        <div className={classes.footer}>
          <div className="container">
            <div className={classes.copyright}>Powered by  <Image src={downArrow} alt='Down Logo' width={36} height={30} className={classes.copyrightLogo} /> </div>
          </div>
        </div>
      </div>
      
		</div>
			
			

			
		</>
    );
}

