import React, { useEffect, useState } from "react";
import Image from "next/image";

import nav from "/public/exhibitions/tabnav.module.scss";
import button from "/public/account/Buttons.module.scss";
import form from "/public/account/Form.module.scss";
import style from "/public/exhibitions/Exhibitions.module.scss";
import Pagination from "@/components/common/paginationtotal.js";
import classes from "../create/exhibitionstyle.module.scss";

// import classes from '../create/customizestyle.module.scss';

import { Button, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faAngleRight,
  faEye,
  faSearch,
  faFont,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";

import ThumbList from "/public/exhibitions/images/thumb.png";

const ExhibitionStyle = () => {
  useEffect(() => {
    if (typeof document !== undefined) {
      require("bootstrap/dist/js/bootstrap");
    }
  }, []);

  const [modalShow, ModalSetShow] = useState(false);
  const handleClose = () => ModalSetShow(false);
  const handleShow = () => ModalSetShow(true);

  const [show, setShow] = useState(true);
  const [show2, setShow2] = useState(false);
  

  return (
    <>
	<div className={`row` + " " + classes.formGroup}>
        <div className="col-md-4">
          <label>Exhibition Thumb Image</label>
        </div>
        <div className="col-md-8">
          <div className={`mb-3` + " " + classes.formControl}>
            <label className={form.chkWrap} onChange={() => setShow(!show)}>
			<input type="checkbox" value="" defaultChecked={true}/>
              <span className={form.chkmark}>Image</span>
            </label>
            {show && (
              <div className={classes.toggleContent}>
                <div className={classes.uploadWrap}>
                  <label className={classes.labelHead}>Select Image</label>
                  <div className="mb-1">
                    <label className={form.radioWrap}>
                      <input
                        type="radio"
                        name="ProfileImg"
                        value=""
                        defaultChecked={true}
                      />{" "}
                      <span className={form.radioLabel}>
                        Upload Your Own Image
                      </span>
                    </label>
                  </div>
                  <div className={classes.UploadIconWrap}>
                    <div className={classes.uploadIcons}>
                      <input type="file" />
                      <FontAwesomeIcon icon={faPlus} size="3x" />
                    </div>
                  </div>
                  <div className="mb-1">
                    <label className={form.radioWrap}>
                      <input type="radio" name="ProfileImg" value="" />{" "}
                      <span className={form.radioLabel}>
                        Choose from our preset images
                      </span>
                    </label>
                  </div>
                  <ul className={classes.imgLIst}>
                    <li className={classes.active}>
                      <label>
                        <input
                          type="radio"
                          name="bgimage"
                          defaultChecked={true}
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="bgimage" />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
            )}
          </div>
          <div className={`mb-2` + " " + classes.formControl}>
            <label className={form.chkWrap} onChange={() => setShow2(!show2)}>
				<input type="checkbox" defaultChecked={true} />{" "}
              <span className={form.chkmark}>Color</span>
            </label>
            {show2 && (
              <div className={classes.toggleContent}>
                <div className={nav.colorpickertab}>
                  <ul className="nav nav-tabs">
                    <li className="nav-item">
                      <a
                        className={nav.navLink + " " + nav.active}
                        data-bs-toggle="tab"
                        href="#solid22"
                      >
                        Soild
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className={nav.navLink}
                        data-bs-toggle="tab"
                        href="#gredient22"
                      >
                        Gradient
                      </a>
                    </li>
                  </ul>
                  <div className="tab-content">
                    <div className="tab-pane show active " id="solid22">
                      <div
                        className={
                          `input-group colorpicker-component colorpicker-element` +
                          " " +
                          nav.colorElement
                        }
                      >
                        <span
                          className={`form-control` + " " + nav.colorBg}
                          style={{ background: "yellow" }}
                        ></span>
                        <input
                          type="text"
                          placeholder="#05FF44"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="tab-pane fade" id="gredient22">
                      <div
                        className={
                          `input-group colorpicker-component colorpicker-element` +
                          " " +
                          nav.colorElement
                        }
                      >
                        <span
                          className={`form-control` + " " + nav.colorBg}
                          style={{
                            background:
                              "linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)",
                          }}
                        ></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className={`text-center` + " " + classes.titleWrap}>
        <h1 className={classes.title}>Customize Exhibition classes</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>
	  <div className={classes.sectionGroup}>
			<div className={classes.titleWrap} style={{ textAlign: "left" }}>
				<h2 className={classes.title}>Selelct Typography</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
		</div>
      <div className={`row` + " " + classes.formGroup}>
        <div className="col-md-4">
          <label>Font Family : Heading</label>
        </div>
        <div className="col-md-7">
          <div
            className={style.formControl}
            variant="primary"
            onClick={handleShow}
          >
            <label
              className={"d-block" + " " + classes.modalLabel}
              style={{ cursor: "pointer" }}
            >
              Select font Family{" "}
              <span style={{ float: "right" }}>
                <FontAwesomeIcon
                  icon={faAngleRight}
                  className={classes.faAngleRight}
                />
              </span>
            </label>
          </div>
        </div>
      </div>
      <div className={`row` + " " + classes.formGroup}>
        <div className="col-md-4">
          <label>Font Family : Text Body</label>
        </div>
        <div className="col-md-7">
          <div
            className={style.formControl}
            variant="primary"
            onClick={handleShow}
          >
            <label
              className={"d-block" + " " + classes.modalLabel}
              style={{ cursor: "pointer" }}
            >
              Select font Family{" "}
              <span style={{ float: "right" }}>
                <FontAwesomeIcon
                  icon={faAngleRight}
                  className={classes.faAngleRight}
                />
              </span>
            </label>
          </div>
        </div>
      </div>
	  <div className={classes.sectionGroup}>
			<div className={classes.titleWrap}>
			<h2 className={classes.title}>Color</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Primary</label>
          </div>
          <div className='col-md-4'>
             
                <div className={classes.formControl}>
                  <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <a className={nav.navLink + ' ' + nav.active} data-bs-toggle="tab" href="#solid">Soild</a>
                    </li>
                    <li className="nav-item">
                        <a className={nav.navLink} data-bs-toggle="tab" href="#gredient">Gradient</a>
                    </li>
                  </ul>
                  <div className="tab-content">
                      <div className="tab-pane show active" id="solid">
                        <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                            <span className={`form-control` + ' ' + nav.colorBg}></span>
                            <input type="text" placeholder="#05FF44" className="form-control" />                            
                        </div>
                      </div>
                      <div className="tab-pane fade" id="gredient">
                        <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                          <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                        </div>
                      </div>
                  </div>
                </div>
              
          </div>
        </div>
		<div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Secondary</label>
          </div>
          <div className='col-md-4'>
                <div className={classes.formControl}>
                  <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <a className={nav.navLink + ' ' + nav.active} data-bs-toggle="tab" href="#solid1">Soild</a>
                    </li>
                    <li className="nav-item">
                        <a className={nav.navLink} data-bs-toggle="tab" href="#gredient1">Gradient</a>
                    </li>
                  </ul>
                  <div className="tab-content">
                      <div className="tab-pane show active" id="solid1">
                        <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                            <span className={`form-control` + ' ' + nav.colorBg}></span>
                            <input type="text" placeholder="#05FF44" className="form-control" />                            
                        </div>
                      </div>
                      <div className="tab-pane fade" id="gredient1">
                        <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                          <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                        </div>
                      </div>
                  </div>
                </div>
              
          </div>
        </div>
        <div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Title Text</label>
          </div>
          <div className='col-md-4'>
                <div className={classes.formControl}>
                  <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <a className={nav.navLink + ' ' + nav.active} data-bs-toggle="tab" href="#solid2">Soild</a>
                    </li>
                    <li className="nav-item">
                        <a className={nav.navLink} data-bs-toggle="tab" href="#gredient2">Gradient</a>
                    </li>
                  </ul>
                  <div className="tab-content">
                      <div className="tab-pane show active" id="solid2">
                        <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                            <span className={`form-control` + ' ' + nav.colorBg}></span>
                            <input type="text" placeholder="#05FF44" className="form-control" />                            
                        </div>
                      </div>
                      <div className="tab-pane fade" id="gredient2">
                        <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                          <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                        </div>
                      </div>
                  </div>
                </div>
              
          </div>
        </div>
        <div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Body Text</label>
          </div>
          <div className='col-md-4'>
        
              <div className={classes.formControl}>
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                      <a className={nav.navLink + ' ' + nav.active} data-bs-toggle="tab" href="#solid3">Soild</a>
                  </li>
                  <li className="nav-item">
                      <a className={nav.navLink} data-bs-toggle="tab" href="#gredient3">Gradient</a>
                  </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane show active" id="solid3">
                      <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                          <span className={`form-control` + ' ' + nav.colorBg} style={{background:"yellow"}}></span>
                          <input type="text" placeholder="#05FF44" className="form-control" />                            
                      </div>
                    </div>
                    <div className="tab-pane fade" id="gredient3">
                      <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                          <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                        </div>
                    </div>
                </div>
              </div>
          
          </div>
        </div>
        <div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Background</label>
          </div>
          <div className='col-md-4'>
           
            <div className={classes.formControl}>
              <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className={nav.navLink+ ' ' + nav.active } data-bs-toggle="tab" href="#solid4">Soild</a>
                </li>
                <li className="nav-item">
                    <a className={nav.navLink} data-bs-toggle="tab" href="#gredient4">Gradient</a>
                </li>
              </ul>
              <div className="tab-content">
                <div className="tab-pane fade show active" id="solid4">
                  <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                      <span className={`form-control` + ' ' + nav.colorBg} style={{background:"yellow"}}></span>
                      <input type="text" placeholder="#05FF44" className="form-control" />                            
                  </div>
                </div>
                <div className="tab-pane fade" id="gredient4">
                  <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                    <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Text with primary BG</label>
          </div>
          <div className='col-md-4'>
      
              <div className={classes.formControl}>
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                      <a className={nav.navLink+ ' ' + nav.active } data-bs-toggle="tab" href="#solid5">Soild</a>
                  </li>
                  <li className="nav-item">
                      <a className={nav.navLink} data-bs-toggle="tab" href="#gredient5">Gradient</a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div className="tab-pane fade show active" id="solid5">
                    <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                        <span className={`form-control` + ' ' + nav.colorBg} style={{background:"yellow"}}></span>
                        <input type="text" placeholder="#05FF44" className="form-control" />                            
                    </div>
                  </div>
                  <div className="tab-pane fade" id="gredient5">
                    <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                      <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                    </div>
                  </div>
                </div>
              </div>
           
          </div>
        </div>
        <div className={`row` + ' ' + classes.formGroup}>
          <div className='col-md-4'>
            <label>Border + Line</label>
          </div>
          <div className='col-md-4'>
            <div className={classes.formGroup}> 
              <div className={classes.formControl}>
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                      <a className={nav.navLink+ ' ' + nav.active } data-bs-toggle="tab" href="#solid6">Soild</a>
                  </li>
                  <li className="nav-item">
                      <a className={nav.navLink} data-bs-toggle="tab" href="#gredient6">Gradient</a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div className="tab-pane fade show active" id="solid6">
                    <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                        <span className={`form-control` + ' ' + nav.colorBg} style={{background:"yellow"}}></span>
                        <input type="text" placeholder="#05FF44" className="form-control" />                            
                    </div>
                  </div>
                  <div className="tab-pane fade" id="gredient6">
                    <div className={`input-group colorpicker-component colorpicker-element` + ' ' + nav.colorElement}>
                      <span className={`form-control` + ' ' + nav.colorBg} style={{background:"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)"}}></span>
                    </div>
                  </div>
                </div>
				<div>
					<label className="mt-3 mb-2">Line Width</label>
					<select className={`form-control ${form.selectDrops}`}>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
						<option>9</option>
						<option>10</option>
					</select>
				</div>		
              </div>
            </div>
          </div>
        </div>
	</div>
      

      {/* Modal Start here  */}

      <Modal
        show={modalShow}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className={classes.modalLg}
      >
        <Modal.Header closeButton style={{ border: "none", paddingBottom: 0 }}>
          <Modal.Title>&nbsp;</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className={style.titleWrap} style={{ textAlign: "left" }}>
            <h2 className={style.title}>Selelct Typography</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div>
          <div className={`row` + " " + style.formGroup}>
            <div
              className={
                `col-md-6` + " " + style.formGroup + " " + classes.search
              }
            >
              <div className={classes.searchModal}>
                <button className="btn">
                  <FontAwesomeIcon
                    icon={faSearch}
                    className={classes.faSearch}
                  />
                </button>
                <input placeholder="Search... " className="form-control" />
              </div>
            </div>
            <div className={`col-md-4` + " " + style.formGroup}>
              <div className={classes.searchModal}>
                <button className="btn">
                  <FontAwesomeIcon icon={faFont} className={classes.faFont} />
                </button>
                <input placeholder="Font Type" className="form-control" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className={classes.fontItemList + " " + classes.active}>
                <div className={classes.checkitem}>
                  <input type="checkbox" />
                  <span className={classes.checkicon}>
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className={classes.faCheckCircle}
                    />
                  </span>
                </div>
                <div className={classes.fontNamePreview}>
                  <div className={classes.topFontStyle}>
                    <div className={classes.fLeft}>
                      <h3>Open Sans</h3>
                      <span>Steve Matteson</span>
                    </div>
                    <div className={classes.fStyle}>Variable</div>
                  </div>
                  <h2>Almost before we knew it, we had left the ground</h2>
                </div>
                <div className={classes.fontBtnSelect}>
                  <h3 className={classes.fontName}>Open Sans</h3>
                  <div className={classes.btnsRow}>
                    <Button
                      className={`btn ${button.btnOutlinePrimary}`}
                      onClick={handleClose}
                    >
                      <FontAwesomeIcon
                        icon={faEye}
                        className={classes.faTrash}
                      />{" "}
                      Preview
                    </Button>
                    <Button
                      className={`btn ${button.btnTextPrimary}`}
                      onClick={handleClose}
                    >
                      Current Select
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className={classes.fontItemList}>
                <div className={classes.checkitem}>
                  <input type="checkbox" />
                  <span className={classes.checkicon}>
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className={classes.faCheckCircle}
                    />
                  </span>
                </div>
                <div className={classes.fontNamePreview}>
                  <div className={classes.topFontStyle}>
                    <div className={classes.fLeft}>
                      <h3>Roboto</h3>
                      <span>Christian Robertson</span>
                    </div>
                    <div className={classes.fStyle}>12 Style</div>
                  </div>
                  <h2>Almost before we knew it, we had left the ground</h2>
                </div>
                <div className={classes.fontBtnSelect}>
                  <h3 className={classes.fontName}>Roboto</h3>
                  <div className={classes.btnsRow}>
                    <Button
                      className={`btn ${button.btnOutlinePrimary}`}
                      onClick={handleClose}
                    >
                      <FontAwesomeIcon
                        icon={faEye}
                        className={classes.faTrash}
                      />{" "}
                      Preview
                    </Button>
                    <Button
                      className={`btn ${button.btnPrimary}`}
                      onClick={handleClose}
                    >
                      Select
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className={classes.fontItemList}>
                <div className={classes.checkitem}>
                  <input type="checkbox" />
                  <span className={classes.checkicon}>
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className={classes.faCheckCircle}
                    />
                  </span>
                </div>
                <div className={classes.fontNamePreview}>
                  <div className={classes.topFontStyle}>
                    <div className={classes.fLeft}>
                      <h3>Roboto</h3>
                      <span>Christian Robertson</span>
                    </div>
                    <div className={classes.fStyle}>12 Style</div>
                  </div>
                  <h2>Almost before we knew it, we had left the ground</h2>
                </div>
                <div className={classes.fontBtnSelect}>
                  <h3 className={classes.fontName}>Roboto</h3>
                  <div className={classes.btnsRow}>
                    <Button
                      className={`btn ${button.btnOutlinePrimary}`}
                      onClick={handleClose}
                    >
                      <FontAwesomeIcon
                        icon={faEye}
                        className={classes.faTrash}
                      />{" "}
                      Preview
                    </Button>
                    <Button
                      className={`btn ${button.btnPrimary}`}
                      onClick={handleClose}
                    >
                      Select
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className={classes.fontItemList}>
                <div className={classes.checkitem}>
                  <input type="checkbox" />
                  <span className={classes.checkicon}>
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className={classes.faCheckCircle}
                    />
                  </span>
                </div>
                <div className={classes.fontNamePreview}>
                  <div className={classes.topFontStyle}>
                    <div className={classes.fLeft}>
                      <h3>Roboto</h3>
                      <span>Christian Robertson</span>
                    </div>
                    <div className={classes.fStyle}>12 Style</div>
                  </div>
                  <h2>Almost before we knew it, we had left the ground</h2>
                </div>
                <div className={classes.fontBtnSelect}>
                  <h3 className={classes.fontName}>Roboto</h3>
                  <div className={classes.btnsRow}>
                    <Button
                      className={`btn ${button.btnOutlinePrimary}`}
                      onClick={handleClose}
                    >
                      <FontAwesomeIcon
                        icon={faEye}
                        className={classes.faTrash}
                      />{" "}
                      Preview
                    </Button>
                    <Button
                      className={`btn ${button.btnPrimary}`}
                      onClick={handleClose}
                    >
                      Select
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className={classes.fontItemList}>
                <div className={classes.checkitem}>
                  <input type="checkbox" />
                  <span className={classes.checkicon}>
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className={classes.faCheckCircle}
                    />
                  </span>
                </div>
                <div className={classes.fontNamePreview}>
                  <div className={classes.topFontStyle}>
                    <div className={classes.fLeft}>
                      <h3>Roboto</h3>
                      <span>Christian Robertson</span>
                    </div>
                    <div className={classes.fStyle}>12 Style</div>
                  </div>
                  <h2>Almost before we knew it, we had left the ground</h2>
                </div>
                <div className={classes.fontBtnSelect}>
                  <h3 className={classes.fontName}>Roboto</h3>
                  <div className={classes.btnsRow}>
                    <Button
                      className={`btn ${button.btnOutlinePrimary}`}
                      onClick={handleClose}
                    >
                      <FontAwesomeIcon
                        icon={faEye}
                        className={classes.faTrash}
                      />{" "}
                      Preview
                    </Button>
                    <Button
                      className={`btn ${button.btnPrimary}`}
                      onClick={handleClose}
                    >
                      Select
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className={classes.fontItemList}>
                <div className={classes.checkitem}>
                  <input type="checkbox" />
                  <span className={classes.checkicon}>
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className={classes.faCheckCircle}
                    />
                  </span>
                </div>
                <div className={classes.fontNamePreview}>
                  <div className={classes.topFontStyle}>
                    <div className={classes.fLeft}>
                      <h3>Roboto</h3>
                      <span>Christian Robertson</span>
                    </div>
                    <div className={classes.fStyle}>12 Style</div>
                  </div>
                  <h2>Almost before we knew it, we had left the ground</h2>
                </div>
                <div className={classes.fontBtnSelect}>
                  <h3 className={classes.fontName}>Roboto</h3>
                  <div className={classes.btnsRow}>
                    <Button
                      className={`btn ${button.btnOutlinePrimary}`}
                      onClick={handleClose}
                    >
                      <FontAwesomeIcon
                        icon={faEye}
                        className={classes.faTrash}
                      />{" "}
                      Preview
                    </Button>
                    <Button
                      className={`btn ${button.btnPrimary}`}
                      onClick={handleClose}
                    >
                      Select
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Pagination />
        </Modal.Body>
        {/* <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer> */}
      </Modal>
    </>
  );
};

export default ExhibitionStyle;
