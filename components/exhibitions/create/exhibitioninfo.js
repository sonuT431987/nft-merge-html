import React, { useEffect, useState } from 'react';

import Image from "next/image"

import form from '/public/account/Form.module.scss';
import nav from "@/global/profile/tabnav.module.scss";

import classes from '@/components/exhibitions/create/exhibitioninfo.module.scss';
import UserAccountStyles from "/public/account/UserAccount.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import ThumbList from '/public/exhibitions/images/thumb.png';


import profileLogo from "/public/dummyassets/profile.png";

const ExhibitionInfo = () => {
  useEffect(() => {
    if (typeof document !== undefined) {
      require("bootstrap/dist/js/bootstrap");
    }
  }, []);

  
  const [show,setShow] = useState(true);
  const [show2,setShow2] = useState(true);
  const [show3, setShow3] = useState(true);
  const [show4, setShow4] = useState(false);
  // useEffect(() => {
  //     document.querySelector("body").classList.add("steptheme");
  // });



  return (
    <>  
      <div className={classes.titleWrap + ' ' + classes.textCenter}>
        <h1 className={classes.title}>Exhibition Info</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>

      <div className={`row` + " " + classes.formGroup}>
        <div className="col-md-4">
          <label>Cover Image</label>
        </div>
        <div className="col-md-8">
          <div className={`mb-2` + " " + classes.formControl}>
            <label
              className={form.chkWrap}
              onChange={() => setShow(!show)}
            >
              <input type="checkbox" defaultChecked={true} />{" "}
              <span className={form.chkmark}>Image</span>
            </label>
            {show && (
              <div className={classes.toggleContent}>
                <div className={classes.uploadWrap}>
                  <label className={classes.labelHead}>
                    Select Image
                  </label>
                  <div className="mb-1">
                    <label className={form.radioWrap}>
                      <input
                        type="radio"
                        name="ProfileImg"
                        value=""
                        defaultChecked={true}
                      />{" "}
                      <span className={form.radioLabel}>
                        Upload Your Own Image
                      </span>
                    </label>
                  </div>
                  <div className={classes.UploadIconWrap}>
                    <div className={classes.uploadIcons}>
                      <input type="file" />
                      <FontAwesomeIcon icon={faPlus} size="3x" />
                    </div>
                  </div>
                  <div className="mb-1">
                    <label className={form.radioWrap}>
                      <input
                        type="radio"
                        name="ProfileImg"
                        value=""
                      />{" "}
                      <span className={form.radioLabel}>
                        Choose from our preset images
                      </span>
                    </label>
                  </div>
                  <ul className={classes.imgLIst}>
                    <li className={classes.active}>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                          defaultChecked={true}
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="radio"
                          name="selectcollectionbg"
                        />
                        <div>
                          <Image
                            src={ThumbList}
                            height={90}
                            width={90}
                            alt="Thumbs"
                          />
                        </div>
                      </label>
                    </li>
                  </ul>

                  <div className={UserAccountStyles.listNote}>
                    Recommended image size: 600 x 600px 
                  </div>
                  <hr className={UserAccountStyles.lineHr} />
                  <label className={UserAccountStyles.labelHead}>
                    Decide how to crop your image
                  </label>
                  <ul className={UserAccountStyles.uploadLists}>
                    <li>
                      <div className={UserAccountStyles.uploadThumb}>
                        <div className={UserAccountStyles.thumbWrap}>
                          <label
                            className={UserAccountStyles.chooseImgs}
                          >
                            <input
                              type="radio"
                              name="profiletwo"
                              value=""
                            />
                            <div
                              className={
                                UserAccountStyles.verticallyImg
                              }
                            >
                              <Image
                                className={
                                  UserAccountStyles.verticallyImg
                                }
                                src={profileLogo}
                                width={50}
                                height={50}
                                alt="Profile Image"
                              />
                            </div>
                          </label>
                        </div>
                        <div
                          className={UserAccountStyles.captionName}
                        >
                          Fill the frame vertically
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className={UserAccountStyles.uploadThumb}>
                        <div className={UserAccountStyles.thumbWrap}>
                          <label
                            className={UserAccountStyles.chooseImgs}
                          >
                            <input
                              type="radio"
                              name="profiletwo"
                              value=""
                            />
                            <div
                              className={
                                UserAccountStyles.horizantalImg
                              }
                            >
                              <Image
                                className={
                                  UserAccountStyles.horizantalImg
                                }
                                src={profileLogo}
                                width={150}
                                height={50}
                                alt="Profile Image"
                              />
                            </div>
                          </label>
                        </div>
                        <div
                          className={UserAccountStyles.captionName}
                        >
                          Fill the frame horizontally
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className={UserAccountStyles.uploadThumb}>
                        <div className={UserAccountStyles.thumbWrap}>
                          <label
                            className={UserAccountStyles.chooseImgs}
                          >
                            <input
                              type="radio"
                              name="profiletwo"
                              value=""
                            />
                            <div
                              className={UserAccountStyles.coverImg}
                            >
                              <Image
                                className={UserAccountStyles.coverImg}
                                src={profileLogo}
                                width={100}
                                height={100}
                                alt="Profile Image"
                              />
                            </div>
                          </label>
                        </div>
                        <div
                          className={UserAccountStyles.captionName}
                        >
                          Cover the frame
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className={UserAccountStyles.uploadThumb}>
                        <div className={UserAccountStyles.thumbWrap}>
                          <label
                            className={UserAccountStyles.chooseImgs}
                          >
                            <input
                              type="radio"
                              name="profiletwo"
                              value=""
                            />
                            <div
                              className={
                                UserAccountStyles.originalImg
                              }
                            >
                              <Image
                                className={
                                  UserAccountStyles.originalImg
                                }
                                src={profileLogo}
                                width={100}
                                height={100}
                                alt="Profile Image"
                              />
                            </div>
                          </label>
                        </div>
                        <div
                          className={UserAccountStyles.captionName}
                        >
                          Original size
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            )}
          </div>
          <div className={classes.formControl}>
            <label
              className={form.chkWrap}
              onChange={() => setShow2(!show2)}
            >
              <input type="checkbox" defaultChecked={true} />{" "}
              <span className={form.chkmark}>Color</span>
            </label>
            {show2 && (
              <div className={classes.toggleContent}>
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                    <a
                      className={nav.navLink + " " + nav.active}
                      data-bs-toggle="tab"
                      href="#solid21"
                    >
                      Soild
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={nav.navLink}
                      data-bs-toggle="tab"
                      href="#gredient21"
                    >
                      Gradient
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div className="tab-pane show active " id="solid21">
                    <div
                      className={
                        `input-group colorpicker-component colorpicker-element` +
                        " " +
                        nav.colorElement
                      }
                    >
                      <span
                        className={`form-control` + " " + nav.colorBg}
                        style={{ background: "yellow" }}
                      ></span>
                      <input
                        type="text"
                        placeholder="#05FF44"
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="tab-pane fade" id="gredient21">
                    <div
                      className={
                        `input-group colorpicker-component colorpicker-element` +
                        " " +
                        nav.colorElement
                      }
                    >
                      <span
                        className={`form-control` + " " + nav.colorBg}
                        style={{
                          background:
                            "linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)",
                        }}
                      ></span>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
	  <div className={`row` + " " + classes.formGroup}>
			<div className="col-md-4">
			<label>Background Image</label>
			</div>
			<div className="col-md-8">
			<div className={`mb-3` + " " + classes.formControl}>
			<label className={form.radioWrap} onChange={() => setShow3(!show3)}>
				<input type="radio" name="BGImg" defaultChecked={true}/>
				<span className={form.radioLabel}>Image</span>
			</label>
				{show3 && (
				<div className={classes.toggleContent}>
					<div className={classes.uploadWrap}>
					<label className={classes.labelHead}>
						Select Image
					</label>
					<div className="mb-1">
						<label className={form.radioWrap}>
						<input
							type="radio"
							name="ProfileImg"
							value=""
							defaultChecked={true}
						/>{" "}
						<span className={form.radioLabel}>
							Upload Your Own Image
						</span>
						</label>
					</div>
					<div className={classes.UploadIconWrap}>
						<div className={classes.uploadIcons}>
						<input type="file" />
						<FontAwesomeIcon icon={faPlus} size="3x" />
						</div>
					</div>
					<div className="mb-1">
						<label className={form.radioWrap}>
						<input
							type="radio"
							name="ProfileImg"
							value=""
						/>{" "}
						<span className={form.radioLabel}>
							Choose from our preset images
						</span>
						</label>
					</div>
					<ul className={classes.imgLIst}>
						<li className={classes.active}>
						<label>
							<input
							type="radio"
							name="bgimage"
							defaultChecked={true}
							/>
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
						<li>
						<label>
							<input type="radio" name="bgimage" />
							<div>
							<Image
								src={ThumbList}
								height={90}
								width={90}
								alt="Thumbs"
							/>
							</div>
						</label>
						</li>
					</ul>
					</div>
					<div className="row mb-3">
					<div className="col-md-6">
						<label>Background-position</label>
					</div>
					<div className="col-md-6">
						<select
						className={
							`form-select` + " " + form.selectDropArrow
						}
						>
						<option value="">Choose Style</option>
						<option value="50px 100px">
							Fill the frame vertically
						</option>
						<option value="10% 40%">
							Fill the frame horizontally
						</option>
						<option value="center top">
							Cover the frame
						</option>
						<option value="right bottom">
							Original size
						</option>
						<option value="left top">Left Top</option>
						<option value="left center">Left Center</option>
						<option value="left bottom">Left Bottom</option>
						<option value="right top">Right Top</option>
						<option value="right center">
							Right Center
						</option>
						</select>
					</div>
					</div>
					<div className="row mb-3">
					<div className="col-md-6">
						<label>Background-repeat</label>
					</div>
					<div className="col-md-6">
						<select
						className={
							`form-select` + " " + form.selectDropArrow
						}
						>
						<option value="">Choose Style</option>
						<option value="repeat">repeat</option>
						<option value="repeat-x">repeat-x</option>
						<option value="repeat-y">repeat-y</option>
						<option value="no-repeat">no-repeat</option>
						<option value="space">space</option>
						<option value="round">round</option>
						<option value="initial">initial</option>
						<option value="inherit">inherit</option>
						</select>
					</div>
					</div>
					<div className="row mb-3">
					<div className="col-md-6">
						<label>Background-size</label>
					</div>
					<div className="col-md-6">
						<select
						className={
							`form-select` + " " + form.selectDropArrow
						}
						>
						<option value="">Choose Style</option>
						<option value="auto">auto</option>
						<option value="length">length</option>
						<option value="percentage">percentage</option>
						<option value="cover">cover</option>
						<option value="contain">contain</option>
						<option value="initial">initial</option>
						<option value="inherit">inherit</option>
						</select>
					</div>
					</div>
				</div>
				)}
			</div>
			<div className={`mb-2` + " " + classes.formControl}>
				<label className={form.radioWrap} onChange={() => setShow4(!show4)}>
					<input type="radio" name="BGImg" value="" />
					<span className={form.radioLabel}>Color</span>
				</label>
				{show4 && (
				<div className={classes.toggleContent}>
					<div className={nav.colorpickertab}>
					<ul className="nav nav-tabs">
						<li className="nav-item">
						<a
							className={nav.navLink + " " + nav.active}
							data-bs-toggle="tab"
							href="#solid22"
						>
							Soild
						</a>
						</li>
						<li className="nav-item">
						<a
							className={nav.navLink}
							data-bs-toggle="tab"
							href="#gredient22"
						>
							Gradient
						</a>
						</li>
					</ul>
					<div className="tab-content">
						<div
						className="tab-pane show active "
						id="solid22"
						>
						<div
							className={
							`input-group colorpicker-component colorpicker-element` +
							" " +
							nav.colorElement
							}
						>
							<span
							className={
								`form-control` + " " + nav.colorBg
							}
							style={{ background: "yellow" }}
							></span>
							<input
							type="text"
							placeholder="#05FF44"
							className="form-control"
							/>
						</div>
						</div>
						<div className="tab-pane fade" id="gredient22">
						<div
							className={
							`input-group colorpicker-component colorpicker-element` +
							" " +
							nav.colorElement
							}
						>
							<span
							className={
								`form-control` + " " + nav.colorBg
							}
							style={{
								background:
								"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)",
							}}
							></span>
						</div>
						</div>
					</div>
					</div>
				</div>
				)}
			</div>
			</div>
		</div>
      <div className={`row` + ' ' + classes.formGroup}>
        <div className='col-md-4'>
          <label>Exhibition Name</label>
        </div>
        <div className='col-md-8'>
            <input type="text" className={form.formElements} placeholder='Name ...  ' />
            <p className={`text-right` + ' ' + classes.leftnumber}>15/80</p>
        </div>
      </div>

      <div className={`row` + ' ' + classes.formGroup}>
        <div className='col-md-4'>
          <label>Description</label>
        </div>
        <div className='col-md-8'>
            <textarea type="text" className={form.formElements} placeholder='Optional ... ' rows="6" ></textarea>
            <p className={`text-right` + ' ' + classes.leftnumber}>15/80</p>
        </div>
      </div>    
        
    </>
  );
}

export default ExhibitionInfo;
