import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './exhibition-assets.module.scss';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Thumb from 'public/exhibitions/exhibition001/images/thumb.png';
//import Thumb from 'public/profile/profile006/images/thumb.png';
import downArrow from 'public/exhibitions/exhibition001/images/down-arrow.png';

import { faShareNodes, faArrowRight, faLink, faPen, faShareAlt, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from "@fortawesome/free-regular-svg-icons";

import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
library.add( faArrowRight, faShareNodes, faHeart, faTwitter, faFacebook, faLink, faPen);


export default function Exhibition001Assets() {
    useEffect(() => {
        document.querySelector("body").classList.add("exhibitionOne");
    });
	

	return (
		<>
  
		<div className={classes.exhibitionAssetsWrap}>
      <div className={classes.exhibitionAssetsInner}>
        <div className={classes.modeListWrap}>
          <div className="container">
            <div className={classes.modeList}>
              <Link href="/exhibitions/exhibition001">
                <a className={classes.modeLink }>VR MODE</a>
              </Link>
              <Link href="/exhibitions/exhibition001/assets">
                <a className={classes.modeLink + ' ' + classes.active}>ASSETS</a>
              </Link> 
            </div>
          </div>
        </div>

        <div className={classes.exhibitionContent}>
          <div className={'container' + ' ' + classes.container}>
            <div className={classes.exbHeadingInfo}>
              <h1 className={classes.title}>Leeds united </h1>
              <p className={classes.userDesc}>
                Leeds United Football Club is an English professional football club based in the city of Leeds, West Yorkshire. The club was formed in 1919 following the disbanding of Leeds City by the Football League and took over their Elland Road stadium
              </p>
            </div>
            <div className={classes.thumblistWrap}>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
              <div className={classes.thumblist}>
                <div className={classes.thumbImg}>
                  <Link className={classes.thumbsItem} href="#">
                    <a>
                      <Image
                      src={Thumb}
                      width={240} height={240}							
                      alt="Thumbs" />
                    </a>							
                  </Link>
                </div>
                <div className={classes.thumbInfo}>            
                  <div className={classes.thumbTitlewrap}>
                    <h3>
                    <Link href="/"><a>Joshua Kimmich</a></Link>
                    </h3>							
                  </div>
                  <p className={classes.thumbDesc}>Midfilelder</p>		
                </div>
              </div>
            </div>

            <div className={classes.paginationWrap}>
              <ul>
                <li className={classes.active}><Link href="#" ><a>1</a></Link></li>
                <li><Link href="#" ><a>2</a></Link></li>
                <li><Link href="#" ><a>3</a></Link></li>
              </ul>
            </div>

          </div>
        </div>

        <div className={classes.footer}>
          <div className="container">
            <div className={classes.copyright}>Powered by  <Image src={downArrow} alt='Down Logo' width={36} height={30} className={classes.copyrightLogo} /> </div>
          </div>
        </div>
      </div>
      
		</div>
			
			

			
		</>
    );
}

