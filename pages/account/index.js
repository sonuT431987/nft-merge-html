import Image from "next/image";
import React, { useEffect, useState } from "react";
import LeftSidebar from "../../components/account/layout/leftsidebar";
import Form from '../../public/account/Form.module.scss';
import nav from '/public/collections/tabnav.module.scss';
import PagesAuth from '@/layout/PageAuth';
import Buttons from '../../public/account/Buttons.module.scss';

import profileLogo from '../../public/dummyassets/profile.png';


import UserAccountStyles from '../../public/account/UserAccount.module.scss';
 
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faArrowRightFromBracket, faCheck, faClone, faPlus } from '@fortawesome/free-solid-svg-icons';
library.add(faCheck, faClone, faAngleLeft, faArrowRightFromBracket, faPlus);



import {
    faFacebook,
    faTwitter,
    faInstagram
  } from "@fortawesome/free-brands-svg-icons";


const UserAccount = (props) => {
	useEffect(() => {
		document.querySelector("body").classList.add("userBg");
	});
	const [show, setShow] = useState(true); 
	const [show2, setShow2] = useState(false);
  return ( 
    <>
	<PagesAuth route={props.route}>
      <div className='userWrap'>		  
		<div className='container'>
			<div className='row'>
				<div className='col-md-3'>
					<LeftSidebar />
				</div>
				<div className='col-md-9'>
					<h3 className={UserAccountStyles.accHead}>My Account</h3>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Profile Image</label>
						<div className="col-md-9">
						<div className={`mb-2` + " " + UserAccountStyles.formControl}>
						<label
							className={Form.chkWrap}
							onChange={() => setShow(!show)}
						>
							<input type="checkbox" defaultChecked={true} />{" "}
							<span className={Form.chkmark}>Image</span>
						</label>
						{show && (
							<div className={UserAccountStyles.toggleContent}>
								<div className={UserAccountStyles.uploadWrap}>
								<label className={UserAccountStyles.labelHead}>Select Image</label>
								<div className="mb-1">
									<label className={Form.radioWrap}><input type="radio" name="ProfileImg" value="" defaultChecked={true}/> <span className={Form.radioLabel}>Upload Your Own Image</span></label>
								</div>
								<div className={UserAccountStyles.UploadIconWrap}>
									<div className={UserAccountStyles.uploadIcons}>
										<input type="file" /><FontAwesomeIcon icon={faPlus} size="3x" />
									</div>
								</div>
								<div className="mb-1">
									<label className={Form.radioWrap}><input type="radio" name="ProfileImg" value="" /> <span className={Form.radioLabel}>Choose from our preset images</span></label>
								</div>
								<ul className={UserAccountStyles.uploadList}>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
									<li><label className={UserAccountStyles.chooseImg}><input type="radio" name="profileOne" value="" /> <div><Image className={UserAccountStyles.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></div></label></li>
								</ul>
								<div className={UserAccountStyles.listNote}>Recommended image size: 1600 x 360px</div>
								<hr className={UserAccountStyles.lineHr} />
								<label className={UserAccountStyles.labelHead}>Decide how to crop your image</label>
								<ul className={UserAccountStyles.uploadLists}>
									<li>
										<div className={UserAccountStyles.uploadThumb}>
											<div className={UserAccountStyles.thumbWrap}>
												<label className={UserAccountStyles.chooseImgs}><input type="radio" name="profiletwo" value="" />
												<div className={UserAccountStyles.verticallyImg}>
													<Image className={UserAccountStyles.verticallyImg} src={profileLogo} width={50} height={50} alt="Profile Image" />
													</div>
												</label>
											</div>
											<div className={UserAccountStyles.captionName}>Fill the frame vertically</div>
										</div>
									</li>
									<li>
										<div className={UserAccountStyles.uploadThumb}>
											<div className={UserAccountStyles.thumbWrap}>
											<label className={UserAccountStyles.chooseImgs}><input type="radio" name="profiletwo" value="" />
												<div className={UserAccountStyles.horizantalImg}>
												<Image className={UserAccountStyles.horizantalImg} src={profileLogo} width={150} height={50} alt="Profile Image" />
												</div>
											</label>
											</div>
											<div className={UserAccountStyles.captionName}>Fill the frame horizontally</div>
										</div>
									</li>
									<li>
										<div className={UserAccountStyles.uploadThumb}>
											<div className={UserAccountStyles.thumbWrap}>
											<label className={UserAccountStyles.chooseImgs}><input type="radio" name="profiletwo" value="" />
											<div className={UserAccountStyles.coverImg}>
												<Image className={UserAccountStyles.coverImg} src={profileLogo} width={100} height={100} alt="Profile Image" />
											</div>
											</label>
											</div>
											<div className={UserAccountStyles.captionName}>Cover the frame</div>
										</div>
									</li>
									<li>
										<div className={UserAccountStyles.uploadThumb}>
											<div className={UserAccountStyles.thumbWrap}>
											<label className={UserAccountStyles.chooseImgs}><input type="radio" name="profiletwo" value="" />
												<div className={UserAccountStyles.originalImg}>
												<Image className={UserAccountStyles.originalImg} src={profileLogo} width={100} height={100} alt="Profile Image" />
												</div>
												</label>
											</div>
											<div className={UserAccountStyles.captionName}>Original size</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						)}
						</div>
							<div className={`mt-3 mb-2` + " " + UserAccountStyles.formControl}>
								<label
									className={Form.chkWrap}
									onChange={() => setShow2(!show2)}
								>
									<input type="checkbox" defaultChecked={true} />{" "}
									<span className={Form.chkmark}>Color</span>
								</label>
								{show2 && (
									<div className={UserAccountStyles.toggleContent}>
									<ul className="nav nav-tabs">
										<li className="nav-item">
										<a
											className={nav.navLink + " " + nav.active}
											data-bs-toggle="tab"
											href="#solid"
										>
											Soild
										</a>
										</li>
										<li className="nav-item">
										<a
											className={nav.navLink}
											data-bs-toggle="tab"
											href="#gredient"
										>
											Gradient
										</a>
										</li>
									</ul>
									<div className="tab-content">
										<div className="tab-pane fade show active " id="solid">
										<div
											className={
											`input-group colorpicker-component colorpicker-element` +
											" " +
											nav.colorElement
											}
										>
											<span
											className={`form-control` + " " + nav.colorBg}
											style={{ background: "yellow" }}
											></span>
											<input
											type="text"
											placeholder="#05FF44"
											className="form-control"
											/>
										</div>
										</div>
										<div className="tab-pane fade" id="gredient">
										<div
											className={
											`input-group colorpicker-component colorpicker-element` +
											" " +
											nav.colorElement
											}
										>
											<span
											className={`form-control` + " " + nav.colorBg}
											style={{
												background:
												"linear-gradient(270.37deg, #57DF96 0.36%, #6D86F1 47.48%, #BF6CFC 96.61%)",
											}}
											></span>
										</div>
										</div>
									</div>
									</div>
								)}
							</div>
						</div>
					</div>
					
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Display Name</label>
						<div className="col-md-6">
							<input type="text" className={`form-control ${Form.formElements}`} placeholder="Jenny Wilson" />
						</div>
					</div>
					<div className="mb-3 row align-items-center">
						<label className="col-md-3 col-form-label">Username</label>
						<div className="col-sm-6">
							<div className="input-group flex-nowrap">
								<input type="text" className={`form-control ${Form.formElements}`} placeholder="Username" />
								<span className="input-group-text"><FontAwesomeIcon icon={faCheck} className="checkTrue" size="lg" /></span>
							</div>
						</div>
						<div className="col-sm-1">
							<button type='button' className='btn btn-link'>Edit</button>
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Connected Wallet</label>
						<div className="col-md-9">
							<div className="input-group flex-nowrap">
								<input type="text" className={`form-control ${Form.formElements}`} placeholder="0xf7A81...613Ff" />
								<span className="input-group-text"><FontAwesomeIcon icon={faClone} className="faClone" size="lg" /></span>
							</div>
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Bio</label>
						<div className="col-md-9">
							<textarea className={`form-control ${Form.formElements}`} placeholder='Tell your fans about your story...'></textarea>
							<div className={UserAccountStyles.charCount}>0/500</div>
						</div>
					</div>
					<hr />
					<h3 className={UserAccountStyles.accHead}>Connect with you</h3>
					<p className={UserAccountStyles.headDesc}>Add  your social networks to allow your fans to connect with you!</p>
					<div className="mb-3 row">
						<label className="col-sm-4 col-form-label"><FontAwesomeIcon icon={faFacebook} size="lg" /> <span className={UserAccountStyles.socialText}>Facebook</span></label>
						<div className="col-sm-8">
							<input type="password" className={`form-control ${Form.formElements}`} placeholder='URL link' />
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-sm-4 col-form-label"><FontAwesomeIcon icon={faTwitter} size="lg" /> <span className={UserAccountStyles.socialText}>Twitter</span></label>
						<div className="col-sm-8">
							<input type="password" className={`form-control ${Form.formElements}`} placeholder='URL link' />
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-sm-4 col-form-label"><FontAwesomeIcon icon={faInstagram} size="lg" /> <span className={UserAccountStyles.socialText}>Instragram</span></label>
						<div className="col-sm-8">
							<input type="password" className={`form-control ${Form.formElements}`} placeholder='URL link' />
						</div>
					</div>
					<hr />
					<h3 className={UserAccountStyles.accHead}>Deactivate account</h3>
					<p className={UserAccountStyles.headDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<div className={UserAccountStyles.btnGroup}>
						<button type='button' className={`btn ${Buttons.btnOutlineDanger}`}>Deactivate</button>
						<div className={UserAccountStyles.btnsWrap}>
							<button type='button' className={`btn ${Buttons.btnOutlinePrimary}`}>Cancel</button>
							<button type='button' className={`btn ${Buttons.btnPrimary}`}>Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div>
	  
	</PagesAuth>
    </>
  );
}
export default UserAccount;