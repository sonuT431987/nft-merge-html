import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import LeftSidebar from "../../components/account/layout/leftsidebar";
import PagesAuth from '@/layout/PageAuth';
import MyAssets001 from "@/components/account/myassets";
import classes from '../../public/account/myassets.module.scss';
import Form from '../../public/account/Form.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';

import Modal from 'react-bootstrap/Modal';
import Modals from '../../public/account/Modals.module.scss';
import Thumbs from '../../public/dummyassets/thumbs.png';
import paginate from '../../public/account/Pagination.module.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

import {faSearch, faEye, faFlag, faArrowsUpDownLeftRight, faUser, faWallet, faTrash, faCircleCheck } from '@fortawesome/free-solid-svg-icons';
library.add(faSearch, faFlag, faEye, faUser, faBitbucket, faWallet, faArrowsUpDownLeftRight, faTrash, faCircleCheck );
import { faBitbucket } from "@fortawesome/free-brands-svg-icons";



export default function Index(props) {
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	
    return ( 
        <>
        <PagesAuth route={props.route}>
            <div className='container'>
				<div className='row'>
					<div className='col-md-3'>
						<LeftSidebar />
					</div>
					<div className='col-md-9'>
						<div className={classes.headWrap}>
							<div>
								<h3 className={classes.head}>My Assets</h3>
							</div>
							<button type="button" className={`btn ${Buttons.btnPrimary}`} onClick={handleShow}>Add Asset</button>
						</div>
						<p className={classes.headNote}>All assets from your connected wallet will display here.</p>
                        <div className='row'>
							<div className={`col-md-4 ${Form.formGroup}`}>
								<div className={`input-group ${Form.inputGroup}`}>
									<span className={`input-group-text ${Form.inputGroupText}`}><FontAwesomeIcon icon={faSearch} size="sm" /></span>
									<input type="text" className={` input-group ${Form.formElements}`} placeholder='Search by NFT name...' />
								</div>
							</div>
							<div className={`col-md-3 ${Form.formGroup}`}>
							<select className={`form-select ${Form.selectDrops}`}>
								<option>Channel</option>
								<option>11</option>
								<option>12</option>
							</select>
							</div>
							<div className={`col-md-3 ${Form.formGroup}`}>
								<select className={`form-select form-group ${Form.selectDrops}`}>
									<option>Contract</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
							<div className={`col-md-2 ${Form.formGroup}`}>
								<select className={`form-select ${Form.selectDrops}`}>
									<option>Sort by date</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
						</div>
						<div className={classes.deleteWrap}><button type="button" className={`btn ${Buttons.btnDanger}`}>Delete Selctions</button></div>
                        <MyAssets001 />
                    </div>
                </div>
            </div>
			<Modal size="xl" show={show} onHide={handleClose}>
				<Modal.Body>
					<h3 className={Modals.head}>All Assets</h3>
					<p className={Modals.headNote}>All assets from your connected wallet will display here.</p>
					<div className='row'>
						<div className={`col-md-4 ${Form.formGroup}`}>
								<div className={`input-group ${Form.inputGroup}`}>
									<span className={`input-group-text ${Form.inputGroupText}`}><FontAwesomeIcon icon={faSearch} size="sm" /></span>
									<input type="text" className={` input-group ${Form.formElements}`} placeholder='Search by NFT name...' />
								</div>
							</div>
							<div className={`col-md-3 ${Form.formGroup}`}>
							<select className={`form-select ${Form.selectDrop}`}>
								<option>10</option>
								<option>11</option>
								<option>12</option>
							</select>
							</div>
							<div className={`col-md-3 ${Form.formGroup}`}>
								<select className={`form-select ${Form.selectDrop}`}>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
							<div className={`col-md-2 ${Form.formGroup}`}>
								<select className={`form-select ${Form.selectDrop}`}>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
						</div>
						<div className={`Wraps ${Modals.popupAsset}`}>
							
							<article className={Modals.thumbsWrap}>
								<label className={Modals.customChk}>
									<input type="checkbox" />
									<span><FontAwesomeIcon icon={faCircleCheck} size="sm" /></span>
								<div>
								<Link className={Modals.thumbsItem} href="">
									<a>
									<Image
										src={Thumbs}
										height={308}
										width={329}
										alt="Thumbs"
									/>
									</a>
								</Link>
								<div className={Modals.thumbsContents}>
									<h3 className={Modals.thumbsTitle}>NFT Name</h3>
									<p className={Modals.thumbsDesc}>
									Desctiption Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Ut molestie maecenas...
									</p>
								</div>
								</div>
								</label>
							</article>
							<article className={Modals.thumbsWrap}>
								<label className={Modals.customChk}>
									<input type="checkbox" />
									<span><FontAwesomeIcon icon={faCircleCheck} size="sm" /></span>
								<div>
								<Link className={Modals.thumbsItem} href="">
									<a>
									<Image
										src={Thumbs}
										height={308}
										width={329}
										alt="Thumbs"
									/>
									</a>
								</Link>
								<div className={Modals.thumbsContents}>
									<h3 className={Modals.thumbsTitle}>NFT Name</h3>
									<p className={Modals.thumbsDesc}>
									Desctiption Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Ut molestie maecenas...
									</p>
								</div>
								</div>
								</label>
							</article>
							<article className={Modals.thumbsWrap}>
								<label className={Modals.customChk}>
									<input type="checkbox" />
									<span><FontAwesomeIcon icon={faCircleCheck} size="sm" /></span>
								<div>
								<Link className={Modals.thumbsItem} href="">
									<a>
									<Image
										src={Thumbs}
										height={308}
										width={329}
										alt="Thumbs"
									/>
									</a>
								</Link>
								<div className={Modals.thumbsContents}>
									<h3 className={Modals.thumbsTitle}>NFT Name</h3>
									<p className={Modals.thumbsDesc}>
									Desctiption Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Ut molestie maecenas...
									</p>
								</div>
								</div>
								</label>
							</article>
						</div>

						<div className={paginate.paginationWrap}>
							<div className={Form.totalFilter}>
								<select className={`form-select ${Form.selectDrop}`}>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>
								<span className={Form.statusCount}>490 Assets</span>
							</div>
							<div className={paginate.paginate}>
								<a href="#" className={paginate.activate}>1</a>
								<a href="#">2</a>
								<a href="#">...</a>
								<a href="#">10</a>
								<a href="#">11</a>
								<a href="#">12</a>
								<a href="#">26</a>
								<a href="#">Next</a>
							</div>
						</div>
						<div className={`text-center ${Buttons.btnsWrap}`}>
							<button className={`btn ${Buttons.btnBorder}`}>Cancel</button>
							<button className={`btn ${Buttons.btnPrimary}`}>Ass Assets</button>
						</div>
			</Modal.Body>
			</Modal>
        </PagesAuth>
    </>
    )
}