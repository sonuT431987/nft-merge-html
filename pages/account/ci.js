import React from 'react';
import CI from '../../public/account/CI.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';
import Typography from '../../public/account/Typography.module.scss';

import PagesAuth from '@/layout/PageAuth';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
library.add(faPlusCircle );

const WebCI = (props) => {
  return (
    <>
        <PagesAuth route={props.route}>
            <div className='container'>
                <h1 className='ciHead'>Light Mode</h1>
                <h4 className='ciSubHead'>Neutral</h4>
                <div className={CI.colWrap}>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModebackground}`}></span>
                        <div>
                        $color-background<br/>#FFFFFF
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModeforegroud}`}></span>
                        <div>
                        $color-foregroud<br/>#FAFAFC
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModedisabled}`}></span>
                        <div>
                        $color-disabled<br/>#EAEAED
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModetextSecondary}`}></span>
                        <div>
                        $color-text-secondary<br/>#A7AAC3
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModetextPrimary}`}></span>
                        <div>
                        $color-text-primary<br/>#3A3A43
                        </div>
                    </div>
                </div>
                <hr />
                <h4 className='ciSubHead'>Primary</h4>
                <div className={CI.colWrap}>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModeprimaryLight}`}></span>
                        <div>
                        $color-primary-light<br/>#F4F7FF
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModeprimaryMain}`}></span>
                        <div>
                        $color-primary-main<br/>#D1DFFF
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModeprimaryDark}`}></span>
                        <div>
                        $color-primary-dark<br/>#154DCC
                        </div>
                    </div>
                </div>
                <h4 className='ciSubHead'>Secondary</h4>
                <div className={CI.colWrap}>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModesecondaryLight}`}></span>
                        <div>
                        $color-secondary-light<br/>#F7FBFF
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModesecondaryMain}`}></span>
                        <div>
                        $color-secondary-main<br/>#DDF0FE
                        </div>
                    </div>
                    <div className={CI.cols}>
                        <span className={`colThumb ${CI.lightModesecondaryDark}`}></span>
                        <div>
                        $color-secondary-dark<br/>#55B6FA
                        </div>
                    </div>
                </div>
                <hr />
                <h1 className='ciHead'>Semantic</h1>
                <div className='row'>
                    <div className='col-sm-6'>
                    <h4 className='ciSubHead'>Info</h4>
                        <div className={CI.colWrap}>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModeinfoBg}`}></span>
                                <div>
                                $color-info-bg<br/>#E8F4FF
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModeinfoMain}`}></span>
                                <div>
                                $color-info-main<br/>#1990FF
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModeinfoContent}`}></span>
                                <div>
                                $color-info-content<br/>#0F5699
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6'>
                        <h4 className='ciSubHead'>Success</h4>
                        <div className={CI.colWrap}>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModesuccessBg}`}></span>
                                <div>
                                $color-success-bg<br/>#F5FDF7
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModesuccessMain}`}></span>
                                <div>
                                $color-success-main<br/>#30CD60
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModesuccessContent}`}></span>
                                <div>
                                $color-success-content<br/>#26A44D
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-6'>
                    <h4 className='ciSubHead'>Warning</h4>
                        <div className={CI.colWrap}>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModewarningBg}`}></span>
                                <div>
                                $color-warning-bg<br/>#FFFBF5
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModewarningMain}`}></span>
                                <div>
                                $color-warning-main<br/>#FFA940
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModewarningDark}`}></span>
                                <div>
                                $color-warning-dark<br/>#CC8733
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6'>
                        <h4 className='ciSubHead'>Error</h4>
                        <div className={CI.colWrap}>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModeerrorBg}`}></span>
                                <div>
                                $color-error-bg<br/>#FDF6F6
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModeerrorMain}`}></span>
                                <div>
                                $color-error-main<br/>#DA4343
                                </div>
                            </div>
                            <div className={CI.cols}>
                                <span className={`colThumb ${CI.lightModeerrorContent}`}></span>
                                <div>
                                $color-error-content<br/>#AE3636
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h1 className="ciHead">Typography - Back Office</h1>
                <hr/>
                <h5>Heading</h5>
                <div className='row'>
                    <div className='col-sm-6 mb-3'><h2 className={`fontWeightBold ${Typography.h2}`}>Open Sans</h2></div>
                    <div className='col-sm-6'><h5 className={Typography.h5}>https://fonts.google.com/&#x200B;specimen/Open+Sans</h5></div>
                </div>
                <h5>Body</h5>
                <div className='row'>
                    <div className='col-sm-6 mb-3'><h2 className={Typography.h2}>Open Sans</h2></div>
                    <div className='col-sm-6'><h5 className={Typography.h5}>https://fonts.google.com/&#x200B;specimen/Open+Sans</h5></div>
                </div>
                <hr />
                <div className='row'>
                    <div className='col-sm-3'>
                        <h1 className={CI.h1Head}>Scale Category</h1>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <h1 className={CI.h1Head}>Typeface</h1>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <h1 className={CI.h1Head}>Style</h1>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <h1 className={CI.h1Head}>Size</h1>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <h1 className={CI.h1Head}>Line Height</h1>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h1 className={Typography.h1}>Headline 1</h1>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>48 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h2 className={Typography.h2}>Headline 2</h2>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>36 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h3 className={Typography.h3}>Headline 3</h3>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>32 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h4 className={Typography.h4}>Headline 4</h4>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>24 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h5 className={Typography.h5}>Headline 5</h5>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>20 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.h6}>Headline 6 Headline 6</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>18 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.subTitleBold}>Subtitle</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>16 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.subTitleNormal}>Subtitle</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Bold<br/>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>16 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.body}>Body</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>14 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.caption}>Caption</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Regular<br/>Light-Italic</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>14 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.overline}>Overlilne</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>12 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.buttonL}>Button L</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>16 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <div className='row mb4'>
                    <div className='col-sm-3'>
                        <h6 className={Typography.buttonS}>Button S</h6>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Open Sans</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Regular</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>14 px</p>
                    </div>
                    <div className='col-sm-2 mb-3'>
                        <p>Auto</p>
                    </div>
                </div>
                <hr/>
                <h1 className="ciHead">Back Office (Light Theme)</h1>
                <h2 className='ciSubHead'>Button - Primary</h2>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'>&nbsp;</div>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'>&nbsp;</div>
                    <div className='col-sm-2 mb-3'>Default</div>
                    <div className='col-sm-2 mb-3'>Hover</div>
                    <div className='col-sm-2 mb-3'>Clicked</div>
                    <div className='col-sm-2 mb-3'>Disabled</div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3'>Button Large</div>
                    <div className='col-sm-2 mb-3'>Primary</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnPrimary}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnPrimaryHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnPrimaryClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnPrimaryDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimary}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimaryHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimaryClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimaryDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextPrimary}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextPrimaryHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextPrimaryClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextPrimaryDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Primary</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixPrimary}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixPrimaryHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixPrimaryClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixPrimarysDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimary}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimaryHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimaryClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlinePrimaryDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextPrimary}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextPrimaryHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextPrimaryClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextPrimarysDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Primary</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixPrimary}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixPrimaryHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixPrimaryClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixPrimarysDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlinePrimary}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlinePrimaryHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlinePrimaryClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlinePrimarysDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextPrimary}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextPrimaryHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextPrimaryClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextPrimarysDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <h2 className='ciSubHead'>Button - Secondary</h2>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'>&nbsp;</div>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'>&nbsp;</div>
                    <div className='col-sm-2 mb-3'>Default</div>
                    <div className='col-sm-2 mb-3'>Hover</div>
                    <div className='col-sm-2 mb-3'>Clicked</div>
                    <div className='col-sm-2 mb-3'>Disabled</div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3'>Button Large</div>
                    <div className='col-sm-2 mb-3'>Secondary</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnSecondary}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnSecondaryHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnSecondaryClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnSecondarysDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineSecondary}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineSecondaryHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineSecondaryClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineSecondaryDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextSecondary}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextSecondaryHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextSecondaryClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextSecondaryDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Secondary</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixSecondary}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixSecondaryHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixSecondaryClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixSecondarysDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline ddad</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineSecondary}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineSecondaryHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineSecondaryClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineSecondarysDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextSecondary}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextSecondaryHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextSecondaryClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextSecondarysDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Secondary</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixSecondary}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixSecondaryHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixSecondaryClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixSecondarysDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineSecondary}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineSecondaryHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineSecondaryClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineSecondarysDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextSecondary}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextSecondaryHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextSecondaryClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextSecondarysDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <h2 className='ciSubHead'>Button - Danger</h2>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'>&nbsp;</div>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'>&nbsp;</div>
                    <div className='col-sm-2 mb-3'>Default</div>
                    <div className='col-sm-2 mb-3'>Hover</div>
                    <div className='col-sm-2 mb-3'>Clicked</div>
                    <div className='col-sm-2 mb-3'>Disabled</div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3'>Button Large</div>
                    <div className='col-sm-2 mb-3'>Danger</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnDanger}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnDangerHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnDangerClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnDangerDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineDanger}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineDangerHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineDangerClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnOutlineDangerDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextDanger}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextDangerHover}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextDangerClicked}`}>BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnTextDangerDesaibled}`}>BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Danger</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixDanger}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixDangerHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixDangerClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixDangersDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineDanger}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineDangerHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineDangerClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixOutlineDangersDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextDanger}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextDangerHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextDangerClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconPrefixTextDangersDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Danger</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixDanger}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixDangerHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixDangerClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixDangersDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Outline</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineDanger}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineDangerHover}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineDangerClicked}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixOutlineDangersDesaibled}`}>BUTTON <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button></div>
                </div>
                <div className='row colmargin'>
                    <div className='col-sm-2 mb-3 d-none d-sm-block'></div>
                    <div className='col-sm-2 mb-3'>Text</div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextDanger}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextDangerHover}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextDangerClicked}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                    <div className='col-sm-2 mb-3'><button type='button' className={`btn ${Buttons.btnIconSuffixTextDangersDesaibled}`}><FontAwesomeIcon icon={faPlusCircle} size="lg" /> BUTTON</button></div>
                </div>

            </div>


        </PagesAuth>
    </>
  );
}

export default WebCI;
