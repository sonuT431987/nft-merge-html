import React, {useEffect} from 'react';
import AuthStyle from './AuthStyle.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';
import Form from '../../public/account/Form.module.scss';
import Image from "next/image";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClose } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';
library.add(faClose, faFacebook, faTwitter, faInstagram);


import profileLogo from '../../public/dummyassets/thumbs.png';



const MoreAuth = () => {
	const [count, setCount] = React.useState(0);
	useEffect( () => { document.querySelector("body").classList.add("AuthWrap") } );
  return (
    <>
		<div className={AuthStyle.headerRight}>
			<div className='container'>
				<div className={AuthStyle.closeWrap}>
					<div className={AuthStyle.closeBtn}><FontAwesomeIcon icon={faClose} size="lg" /></div>
				</div>
			</div>
		</div>
		<div className={AuthStyle.wraper}>
			<h1 className={AuthStyle.head}>More about you</h1>
			<h5 className={AuthStyle.subHead}>Upload your coolest profile image<span className="lineBreak"></span>And tell more about you</h5>
			<div className={AuthStyle.uploadWrap}>
				<div className={AuthStyle.uploadimg}>
					<Image src={profileLogo} alt='Logo' width={128} height={128} />
				</div>
				<div className={AuthStyle.uploadtext}>
					<div className={`${Buttons.uploadBtn}`}>
						<input type="file" />
						<button type='button' className={`btn ${Buttons.btnOutlinePrimary}`}>Upload</button>
					</div>
					<div className={AuthStyle.uploadmsg}>Image should be......(spec)</div>
				</div>
			</div>
			<h5 className={AuthStyle.paraHeadOne}>Bio (Optional)</h5>
			<div className={`${Form.formGroup}`}>
				<textarea  className={`form-control nobgelements ${Form.formElements}`} placeholder="I’m a coolest artist." onChange={e => setCount(e.target.value.length)} />
				<div className={AuthStyle.charCount}>{count}0/500</div>
			</div>
			<h5 className={AuthStyle.paraHeadOne}>Social Media (Optional)</h5>
			<div className={`${Form.formGroup}`}>
				<label>Facebook</label>
				<div className="input-group flex-nowrapleft">
				<span className={`input-group-text` + ' ' + AuthStyle.inputGroupText}><FontAwesomeIcon icon={faFacebook} className="checkTrue" size="lg" /></span>
					<input type="text" className={`form-control nobgelements ${Form.formElements}`} placeholder="www.facebook.com/username" />
				</div>
				<div className={AuthStyle.charCount}>0/500</div>
			</div>
			<div className={`${Form.formGroup}`}>
				<label>Twitter</label>
				<div className="input-group flex-nowrapleft">
				<span className={`input-group-text` + ' ' + AuthStyle.inputGroupText}><FontAwesomeIcon icon={faTwitter} className="checkTrue" size="lg" /></span>
					<input type="text" className={`form-control nobgelements ${Form.formElements}`} placeholder="www.twitter.com/username" />
				</div>
				<div className={AuthStyle.charCount}>0/500</div>
			</div>
			<div className={`${Form.formGroup}`}>
				<label>Instagram</label>
				<div className="input-group flex-nowrapleft">
				<span className={`input-group-text` + ' ' + AuthStyle.inputGroupText}><FontAwesomeIcon icon={faInstagram} className="checkTrue" size="lg" /></span>
					<input type="text" className={`form-control nobgelements ${Form.formElements}`} placeholder="www.instagram.com/username" />
				</div>
				<div className={AuthStyle.charCount}>0/500</div>
			</div>

			<div className={AuthStyle.btnWrapMore}>
				<button type='button' className={`btn ${Buttons.btnGray}`}>NEXT</button>
			</div>
		</div>

    </>
  );
}

export default MoreAuth;



