import React, {useEffect} from 'react';
import AuthStyle from './AuthStyle.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';
import Image from "next/image";
import Link from 'next/link';

import HomeLogo from '../../public/main/images/homeLogo.svg';
import MetaLogo from '../../public/main/images/metamaskIcons.svg';
import BitkubLogo from '../../public/main/images/butkubIcons.svg';
 
const Auth = () => {
	useEffect( () => { document.querySelector("body").classList.add("AuthWrap") } );
  return (
    <>
		<header>
			<div className='container'>
				<div className={AuthStyle.headerRow}>
					<div className={AuthStyle.logo}>
						<Image src={HomeLogo} alt='Logo' width={32} height={32} />
					</div>
					<nav className={AuthStyle.navigations}>
						<Link href="/"><a>Home</a></Link>
						<Link href="/"><a>About</a></Link>
						<Link href="/"><a>FAQ</a></Link>
					</nav>
				</div>
			</div>
		</header>
		<div className={AuthStyle.wraper}>
			<h1 className={AuthStyle.head}>Connect your Wallet</h1>
			<h5 className={AuthStyle.subHead}>Access the Credit Spaces<span className="lineBreak"></span> using your favourite Wallet!</h5>
			<div className={AuthStyle.authWrap}>
				<button type="button" className={`btn w-100 ${Buttons.btnLogin}`}><Image src={MetaLogo} alt='Logo' width={36} height={36} /> Connect with Metamask</button>
			</div>
			<div className={AuthStyle.authWrap}>
				<button type="button" className={`btn w-100 ${Buttons.btnLogin}`}><Image src={BitkubLogo} alt='Logo' width={36} height={36} /> Connect with Metamask</button>
			</div>
			<h5 className={AuthStyle.paraHead}>We only detect your wallet address to fetch your assets. We do not access the wallet or perform any operation with it.</h5>
		</div>

    </>
  );
}

export default Auth;



