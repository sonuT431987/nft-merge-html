import React, {useEffect} from 'react';
import AuthStyle from './AuthStyle.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';
import Form from '../../public/account/Form.module.scss';
import Image from "next/image";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faArrowRightFromBracket, faAt, faCheck, faSpinner, faUser } from '@fortawesome/free-solid-svg-icons';
library.add(faAngleLeft, faArrowRightFromBracket, faUser, faAt, faCheck, faSpinner);



import MetaMask from '../../public/main/images/metamask.svg';
import MetaBitkub from '../../public/main/images/bitkubchain.svg';
import CreaterLogo from '../../public/main/images/createlogo.png';
import profileLogo from '../../public/dummyassets/profile.png';
import downArrow from '../../public/dummyassets/down-arrow.svg';



const WalletConnect = () => {
	useEffect( () => { document.querySelector("body").classList.add("AuthWrap") } );
  return (
    <>
		<div className={AuthStyle.headerRight}>
			<div className='container'>
				<div className={AuthStyle.userLogged}>
					<div className={AuthStyle.userImg}>
						<Image className={AuthStyle.profileImg} src={profileLogo} width={48} height={48} alt="Profile Image" />
					</div>
					<div className={AuthStyle.userInfo}>
						<h4 className={AuthStyle.userTitle}>Jenny Wilson</h4>
						<div className={AuthStyle.userId}>0xf7A81...613Ff</div>
					</div>
					<button type='button' className={`btn ${Buttons.btnOutlinePrimary}`}><FontAwesomeIcon icon={faArrowRightFromBracket} size="lg" /></button>
				</div>
			</div>
		</div>
		<div className={AuthStyle.wraper}>
			<h1 className={AuthStyle.head}>Wallet Connected</h1>
			<h5 className={AuthStyle.paraHead}>Wallet</h5>
			<div className={AuthStyle.listWrap}>
				<div className={AuthStyle.connectList}><Image src={MetaBitkub} alt='Logo' width={18} height={18} /> <span>Bitkub Chain</span></div>
				<div className={AuthStyle.connectList}><Image src={MetaMask} alt='Logo' width={18} height={18} /> <span>0xceB94548aC987CE1d8a0B760dec671A6Bd3c8D5D</span></div>
			</div>
			<h5 className={AuthStyle.paraHead}>Profile Setting</h5>
			<div className={`${Form.formGroup}`}>
				<label>Name</label>
				<div className="input-group flex-nowrapleft">
					<span className="input-group-text"><FontAwesomeIcon icon={faUser} className="checkTrue" size="lg" /></span>
					<input type="text" className={`form-control nobgelements ${Form.formElements}`} placeholder="name" />
				</div>
				<div className={AuthStyle.charCount}>0/15</div>
			</div>
			<div className={`${Form.formGroup}`}>
				<label>Username</label>
				<div className="input-group flex-nowrapboth">
					<span className="input-group-text"><FontAwesomeIcon icon={faAt} className="checkTrue" size="lg" /></span>
					<input type="text" className={`form-control nobgelements ${Form.formElements}`} placeholder="Username" />
					<span className={`input-group-text bg-transparent ${Form.formLoader}`}><div className='loading'></div>&nbsp;&nbsp;<FontAwesomeIcon icon={faCheck} className="checkTrue" size="lg" /></span>
				</div>
				<div className="formUserStatus success">User is available</div>
				<div className={AuthStyle.charCount}>0/15</div>
			</div>

			<h5 className={AuthStyle.paraHead}>Join as creator</h5>
			<p>สร้าง Art Gallery กับ NFTIFY เพื่อโปรโมทผลงานและของสะสมของคุณ<span className="lineBreak"></span> ด้วย Profile Theme ที่สามารถปรับแต่งได้ตามต้องการ </p>
			<div className={AuthStyle.CreaterLogo}>
				<Image src={CreaterLogo} alt='Logo' width={512} height={204} />
			</div>
			<div className={AuthStyle.btnWrap}>
				<button type='button' className={`btn ${Buttons.btnPrimary}`}>Join now</button>
			</div>
			<div className='text-center mt-4'><button type='button' className={`btn ${Buttons.btnTextDanger}`}>Deactivate account</button></div>
		</div>
		<div className={AuthStyle.footer}>
			<div className="container">
			<div className={AuthStyle.copyright}>Powered by NFTIFY <Image src={downArrow} alt='Down Logo' width={50} height={30} /> </div>
			</div>
      	</div>
    </>
  );
}

export default WalletConnect;



