import React, {useEffect} from 'react';
import AuthStyle from './AuthStyle.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';
import Image from "next/image";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faCircleCheck, faClose } from '@fortawesome/free-solid-svg-icons';
library.add(faAngleLeft, faCircleCheck);


import Slider from "react-slick";


import SlickImg from '../../public/main/images/thumbsfull.png';


const WalletConnect = () => {
	useEffect( () => { document.querySelector("body").classList.add("AuthWrap") } );

	const sliderSettings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
				arrows: false,
				slidesToShow: 1,
				variableWidth: false,
			}
			} 
		]
	};

  return (
    <>
		<div className={AuthStyle.headerRight}>
			<div className='container'>
				<div className={AuthStyle.closeBtnWrap}>
					<span className={AuthStyle.closeBtn}><FontAwesomeIcon icon={faClose} size="lg" /></span>
				</div>
			</div>
		</div>
		<div className={AuthStyle.wraper}>
			<h1 className={AuthStyle.head}>Welcome to NFTIFY</h1>
			<h5 className={AuthStyle.subHead}>Are you ready to experience of NFT?<span className="lineBreak"></span> Select your favourite profile theme.</h5>
			<div className={AuthStyle.sliderList}>
				<Slider {...sliderSettings}>
					<div className={AuthStyle.thumbsWrap}>
						<label className={AuthStyle.chooseImg}>
							<input type="radio" name="profileTheme" value="" />
							<div className={AuthStyle.selectIcons}><FontAwesomeIcon icon={faCircleCheck} size="3x" /></div>
							<div className={AuthStyle.chooseThumb}>
								<Image src={SlickImg} alt='Logo' width={512} height={376} />
								<h3 className={AuthStyle.caption}>Default</h3>
							</div>
						</label>
					</div>
					<div className={AuthStyle.thumbsWrap}>
						<label className={AuthStyle.chooseImg}>
							<input type="radio" name="profileTheme" value="" />
							<div className={AuthStyle.selectIcons}><FontAwesomeIcon icon={faCircleCheck} size="3x" /></div>
							<div className={AuthStyle.chooseThumb}>
								<Image src={SlickImg} alt='Logo' width={512} height={376} />
								<h3 className={AuthStyle.caption}>Default</h3>
							</div>
						</label>
					</div>
					<div className={AuthStyle.thumbsWrap}>
						<label className={AuthStyle.chooseImg}>
							<input type="radio" name="profileTheme" value="" />
							<div className={AuthStyle.selectIcons}><FontAwesomeIcon icon={faCircleCheck} size="3x" /></div>
							<div className={AuthStyle.chooseThumb}>
								<Image src={SlickImg} alt='Logo' width={512} height={376} />
								<h3 className={AuthStyle.caption}>Default</h3>
							</div>
						</label>
					</div>
					<div className={AuthStyle.thumbsWrap}>
						<label className={AuthStyle.chooseImg}>
							<input type="radio" name="profileTheme" value="" />
							<div className={AuthStyle.selectIcons}><FontAwesomeIcon icon={faCircleCheck} size="3x" /></div>
							<div className={AuthStyle.chooseThumb}>
								<Image src={SlickImg} alt='Logo' width={512} height={376} />
								<h3 className={AuthStyle.caption}>Default</h3>
							</div>
						</label>
					</div>
				</Slider>
			</div>
			<div className={AuthStyle.btnWrap}>
				<button type='button' className={`btn ${Buttons.btnPrimary}`}>NEXT</button>
			</div>
		</div>
    </>
  );
}

export default WalletConnect;



