import React, { useEffect } from "react";
import Link from "next/link";
import Image from "next/image";


import Header from '@/components/common/header';
import Footer from '@/components/common/footer';
import Buttons from 'public/account/Buttons.module.scss';

import { useWeb3 } from "@3rdweb/hooks";




export default function Home() {
	useEffect( () => { document.querySelector("body").classList.add("mainHome") } );
	const { address, connectWallet, disconnectWallet } = useWeb3();
	return (
	<>
			<div className="contentwrap">
				<Header />
				<div className="banners-wapper">
					<div className="banners-wrap">
						<div className="container">
							<div className="row align-items-center">
								<div className="col-sm-6 bannercolone form-group">
									<div className="bannertop-wrap">
										<h3 className="bannertop-head">Spotlight your valuable<br/>NFT Collection</h3>
										<p className="bannertop-para">พื้นที่สำหรับการโชว์ผลงาน NFT และ Collections ในรูปแบบของ Online Virtual Spaces สามารถรับชมได้ทุกที่ทุกเวลา เพียงแค่เชื่อม Wallet ของคุณกับ NFTIFY </p>
										<div className="btn-bannertop"><button type="button" className="btn">Create Your Virtual Space</button></div>
									</div>
								</div>
								<div className="col-sm-5 bannercoltwo offset-sm-1 form-group">
									<img src="images/banner-icons.png" alt="Amplify" />
								</div>
							</div>
						</div>
					</div>
					<div className="banners-wraps">
						<div className="container">
							<div className="row align-items-center">
								<div className="col-sm-6 form-group">
								<img src="images/amplify.png" alt="Amplify" />

								</div>
								<div className="col-sm-5 offset-sm-1 form-group">
									<div className="amplify-wrap">
										<h3 className="amplify-head">You create, we amplify</h3>
										<p className="amplify-para">Because you NFT is special on its own, with NFINITE we help amplifying & magnificallzing your NFT’s value to be a skyrocket asset at zero cost.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tinciduntLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
		

				<div className="profile-wrap">
					<div className="container">
						<div className="numwrap">
							<div className="showcase-num"><span className="num"><img src="images/1-num.png" alt="Third Steps" /></span></div>
							<h2 className="sub-section">Profile showcase</h2>
							<p className="num-para">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
						</div>
						<div className="grid-lists">
							<ul className="grid-wrap">
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/profile-one.png" alt="One Steps" />
										</div>
										<h4 className="thumb-head">Center Profile</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/profile-two.png" alt="Two Steps" />
										</div>
										<h4 className="thumb-head">Album</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/profile-three.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Left Image</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/profile-four.png" alt="Four Steps" />
										</div>
										<h4 className="thumb-head">Left Menu</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/profile-five.png" alt="Five Steps" />
										</div>
										<h4 className="thumb-head">Modular Grid</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/profile-six.png" alt="Six Steps" />
										</div>
										<h4 className="thumb-head">Book</h4>
									</div>
								</li>
							</ul>
							<div className="view-all">
								<button type="button" className="btn btn-view">View all</button>
							</div>
						</div>
					</div>
				</div>
				<div className="wallet-wrapps">
					<div className="container">
					<div className="numwrap">
						<div className="showcase-num"><span className="num"><img src="images/2-num.png" alt="Third Steps" /></span></div>
						<h2 className="sub-section">Connect your wallet</h2>
						<p className="num-para">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
					</div>
					<div className="wallet-icons"><img src="images/wallet-icons.png" alt="Wallet Icons" /></div>
						<div className="numwrap">
							<div className="showcase-num line"><span className="num-line"></span></div>
							<h2 className="sub-section">Collection showcase</h2>
							<p className="num-para">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
						</div>
						<div className="grid-lists">
							<ul className="grid-wrap">
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/collection-one.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Center Profile</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/collection-two.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Album</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/collection-three.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Left Image</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/collection-four.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Left Menu</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/collection-five.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Modular Grid</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/collection-six.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Book</h4>
									</div>
								</li>
							</ul>
							<div className="view-all">
								<button type="button" className="btn btn-view">View all</button>
							</div>
						</div>
					</div>
				</div>

				<div className="showcase-wrapper">
					<div className="container">
						<div className="numwrap">
							<div className="showcase-num"><span className="num"><img src="images/3-num.png" alt="Third Steps" /></span></div>
							<h2 className="sub-section">Exhibition showcase</h2>
							<p className="num-para">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
						</div>
						<div className="grid-lists">
							<ul className="grid-wrap">
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/showcase-one.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Template Name (Default)</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/showcase-two.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Classic Gallery</h4>
									</div>
								</li>
								<li>
									<div className="grid-thums">
										<div className="grid-img">
											<img src="images/showcase-three.png" alt="Third Steps" />
										</div>
										<h4 className="thumb-head">Left Image</h4>
									</div>
								</li>
							</ul>
							<div className="view-all">
								<button type="button" className="btn btn-view">View all</button>
							</div>
						</div>
						<div className="case-collection">
							<h1 className="collection-head">Show Case Your NFTs and Collections</h1>
							<p className="collection-para">สร้างนิทรรศการออนไลน์ของคุณกับ NFTIFY<br />เพื่อโชว์ผลงาน NFT และของสะสม</p>
							<button type="button" className="btn btn-primary case-btn">Create Your  Virtual Collection</button>
						</div>
						<div className="faq-wrap">
							<h2 className="sub-sectionleft">FAQs</h2>
							<div className="row">
								<div className="col-sm-6 form-group">
									Everything you need tp know about <strong>NFINITE</strong>
								</div>
								<div className="col-sm-6 form-group">
									<div className="faqwrapper">
										<label className="task-collapse">
											<input type="checkbox" className="toggle-collapse" />
											<div className="heading">What&lsquo;s NFINITE? How do I get it?</div>
											<div className="collapses">
												<div className="contents">Just like you can connect your NFINITE collection to a social account, you&lsquo;ll now be able to connect your Twitter to your profile! This adds another layer of authentication for users on NFINITE.</div>        
											</div>
										</label>
										<label className="task-collapse">
											<input type="checkbox" className="toggle-collapse" />
											<div className="heading">How can I connect my Twitter account to my profile?</div>
											<div className="collapses">
												<div className="contents">Just like you can connect your NFINITE collection to a social account, you&lsquo;ll now be able to connect your Twitter to your profile! This adds another layer of authentication for users on NFINITE.</div>        
											</div>
										</label>
										<label className="task-collapse">
											<input type="checkbox" className="toggle-collapse" />
											<div className="heading">How can I connect my social account with my collection?</div>
											<div className="collapses">
												<div className="contents">Just like you can connect your NFINITE collection to a social account, you&lsquo;ll now be able to connect your Twitter to your profile! This adds another layer of authentication for users on NFINITE.</div>        
											</div>
										</label>
										<label className="task-collapse">
											<input type="checkbox" className="toggle-collapse" />
											<div className="heading">How do I find my asserts? </div>
											<div className="collapses">
												<div className="contents">Just like you can connect your NFINITE collection to a social account, you&lsquo;ll now be able to connect your Twitter to your profile! This adds another layer of authentication for users on NFINITE.</div>        
											</div>
										</label>
										<label className="task-collapse">
											<input type="checkbox" className="toggle-collapse" />
											<div className="heading">How can I contact NFINITE?</div>
											<div className="collapses">
												<div className="contents">Just like you can connect your NFINITE collection to a social account, you&lsquo;ll now be able to connect your Twitter to your profile! This adds another layer of authentication for users on NFINITE.</div>        
											</div>
										</label>
									</div>
									<div className="faq-all"><Link href="#"><a>more questions</a></Link></div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<Footer />
			</div>
	</>
  )
}

