import React, { useState, useEffect, useContext } from "react";
import Profile001 from "@/components/profile/profile001";
import Profile002 from "@/components/profile/profile002";
import Profile003 from "@/components/profile/profile003";
import Profile004 from "@/components/profile/profile004";
import Profile005 from "@/components/profile/profile005";
import Profile006 from "@/components/profile/profile006";
import Profile007 from "@/components/profile/profile007";
import Profile008 from "@/components/profile/profile008";
import Profile009 from "@/components/profile/profile009";
import PageMain from '@/layout/PageMain'
const SlugProfile = (props) => {
    return (
        <PageMain route={props.user}>
            <Profile001/>
        </PageMain>
    )
}
export default SlugProfile;