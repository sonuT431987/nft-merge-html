import Link from 'next/link';
import Image from "next/image";
import React, { useEffect, useState } from "react";
import PagesAuth from '@/layout/PageAuth';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";


import LeftSidebar  from '@/components/account/layout/leftsidebar.js';
import CI from 'public/account/CI.module.scss';
import Modal from 'react-bootstrap/Modal';
import Modals from '../../../public/account/Modals.module.scss';

// import form from '../../../public/account/Form.module.scss';

import button from 'public/account/Buttons.module.scss';
import Pagination from '@/components/common/pagination.js';

import classes from 'public/profile/theme/index.module.scss';
import nav from 'public/profile/tabnav.module.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThLarge, faPlus, faAngleRight, faSearch, faFont, faCheckCircle, faEye  } from '@fortawesome/free-solid-svg-icons';


import ThemeSlider1 from '../../../public/profile/profile001/images/preview/profile001-1.png';
import ThemeSlider2 from '../../../public/profile/profile001/images/preview/profile001-2.png';
import ThemeSlider3 from '../../../public/profile/profile001/images/preview/profile001-3.png';
import ThemeSlider4 from '../../../public/profile/profile001/images/preview/profile001-4.png';



import Theme1 from 'public/profile/images/thumblist.png';
import Theme2 from 'public/profile/images/thumblist2.png';
import Theme3 from 'public/profile/images/thumblist3.png';
import Theme4 from 'public/profile/images/thumblist4.png';
import Theme5 from 'public/profile/images/thumblist5.png';
import Theme6 from 'public/profile/images/thumblist6.png';


const sliderSettings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
	// responsive: [
	// 	{
	// 	breakpoint: 1024,
	// 	settings: {
	// 		slidesToShow: 1,
	// 		slidesToScroll: 1,
	// 	}
	// 	},
	// 	{
	// 	breakpoint: 600,
	// 	settings: {
	// 		slidesToShow: 3,
	// 		slidesToScroll: 2,
	// 		initialSlide: 2
	// 	}
	// 	},
	// 	{
	// 	breakpoint: 480,
	// 	settings: {
	// 		slidesToShow: 2,
	// 		slidesToScroll: 1
	// 	}
	// 	}
	// ]
};
export default function Index(props) {
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	useEffect(() => {
        if (typeof document !== undefined) {
          require("bootstrap/dist/js/bootstrap");
        }
    }, []);

    useEffect(() => {
        document.querySelector("body").classList.add("collectionThemeListpage");
    });


    

  return ( 
    <>
    <PagesAuth route={props.user}>
      <div className='container'>
        <div className={classes.titleWrap + ' ' + classes.textCenter}>
          <h1 className={classes.title}>Please select collection theme</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>

        <ul className={`nav nav-tabs `+ classes.tab}>
          <li className="nav-item">
            <a className={nav.navLink + ' ' + nav.active}  data-bs-toggle="tab" href="#category1">Category A</a>
          </li>
          <li className="nav-item">
            <a className={nav.navLink} data-bs-toggle="tab" href="#category2">Category B</a>
          </li>
          <li className="nav-item">
            <a className={nav.navLink} data-bs-toggle="tab" href="#category3">Category C</a>
          </li>
        </ul>

        <div className="tab-content">
          <div className="tab-pane show active" id="category1">
            <div className='row'>
              <div className='col-6 col-lg-4'> 
                <div className={classes.thumblist + ' ' + classes.active}>
                  <div className={classes.thumbImg}>
                    <label className={classes.checkitem + ' ' + classes.customChk }>
                      <Image
                        src={Theme1}
                        height={412}
                        width={412}
                        alt="Thumbs"
                      />   
                      <input type="checkbox" defaultChecked={true} />
                      <span className={classes.checkicon}>                   
                      <FontAwesomeIcon icon={faCheckCircle} className={classes.faCheckCircle} />
                      </span>
                    </label>
                  </div>
                  <div className={classes.thumbInfo}>                 
                    <h3 className={classes.thumbTitle}>
                      <Link href="/"><a>Template Name (Default)</a></Link>
                    </h3>                  
                    <div className={classes.btnGroup}>
                      <button className={`btn ${button.btnOutlinePrimary + ' ' + classes.btn}`} onClick={handleShow}>
                        <FontAwesomeIcon icon={faEye} className={classes.faeye} />
                        Preview
                      </button>
                      <button className={`btn ${button.btnDefault + ' ' + classes.btn}`}>
                      <input type="radio" name="themeselect" />
                      Current Template
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-6 col-lg-4'>
                <div className={classes.thumblist}>
                  <div className={classes.thumbImg}>
                    <label className={classes.checkitem + ' ' + classes.customChk }>
                      <Image
                        src={Theme2}
                        height={412}
                        width={412}
                        alt="Thumbs"
                      />                  
                      <input type="checkbox" />
                      <span className={classes.checkicon}>                   
                      <FontAwesomeIcon icon={faCheckCircle} className={classes.faCheckCircle} />
                      </span>
                    </label>
                  </div>
                  <div className={classes.thumbInfo}>                 
                    <h3 className={classes.thumbTitle}>
                      <Link href="/"><a>Template Name</a></Link>
                    </h3>
                    <div className={classes.btnGroup}>
                      <button className={`btn ${button.btnOutlinePrimary + ' ' + classes.btn}`} onClick={handleShow}>
                        <FontAwesomeIcon icon={faEye} className={classes.faeye} />
                        Preview
                      </button>
                      <button className={`btn ${button.btnPrimary + ' ' + classes.btn}`}>
                      <input type="radio" name="themeselect" />
                        Select
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-6 col-lg-4'>
                <div className={classes.thumblist}>
                  <div className={classes.thumbImg}>
                    <label className={classes.checkitem + ' ' + classes.customChk }>
                      <Image
                        src={Theme3}
                        height={412}
                        width={412}
                        alt="Thumbs"
                      />                  
                      <input type="checkbox" />
                      <span className={classes.checkicon}>                   
                      <FontAwesomeIcon icon={faCheckCircle} className={classes.faCheckCircle} />
                      </span>
                    </label>
                  </div>
                  <div className={classes.thumbInfo}>                 
                    <h3 className={classes.thumbTitle}>
                      <Link href="/"><a>Template Name </a></Link>
                    </h3>
                    <div className={classes.btnGroup}>
                      <button className={`btn ${button.btnOutlinePrimary + ' ' + classes.btn}`} onClick={handleShow}>
                        <FontAwesomeIcon icon={faEye} className={classes.faeye} />
                        Preview
                      </button>
                      <button className={`btn ${button.btnPrimary + ' ' + classes.btn}`}>
                      <input type="radio" name="themeselect" />
                        Select
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-6 col-lg-4'>
                <div className={classes.thumblist}>
                  <div className={classes.thumbImg}>
                    <label className={classes.checkitem + ' ' + classes.customChk }>
                      <Image
                        src={Theme4}
                        height={412}
                        width={412}
                        alt="Thumbs"
                      />                  
                      <input type="checkbox" />
                      <span className={classes.checkicon}>                   
                      <FontAwesomeIcon icon={faCheckCircle} className={classes.faCheckCircle} />
                      </span>
                    </label>
                  </div>
                  <div className={classes.thumbInfo}>                 
                    <h3 className={classes.thumbTitle}>
                      <Link href="/"><a>Template Name </a></Link>
                    </h3>
                    <div className={classes.btnGroup}>
                      <button className={`btn ${button.btnOutlinePrimary + ' ' + classes.btn}`} onClick={handleShow}>
                        <FontAwesomeIcon icon={faEye} className={classes.faeye} />
                        Preview
                      </button>
                      <button className={`btn ${button.btnPrimary + ' ' + classes.btn}`}>
                      <input type="radio" name="themeselect" />
                        Select
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-6 col-lg-4'>
                <div className={classes.thumblist}>
                  <div className={classes.thumbImg}>
                    <label className={classes.checkitem + ' ' + classes.customChk }>
                      <Image
                        src={Theme5}
                        height={412}
                        width={412}
                        alt="Thumbs"
                      />                  
                      <input type="checkbox" />
                      <span className={classes.checkicon}>                   
                      <FontAwesomeIcon icon={faCheckCircle} className={classes.faCheckCircle} />
                      </span>
                    </label>
                  </div>
                  <div className={classes.thumbInfo}>                 
                    <h3 className={classes.thumbTitle}>
                      <Link href="/"><a>Template Name</a></Link>
                    </h3>
                    <div className={classes.btnGroup}>
                      <button className={`btn ${button.btnOutlinePrimary + ' ' + classes.btn}`} onClick={handleShow}>
                        <FontAwesomeIcon icon={faEye} className={classes.faeye} />
                        Preview
                      </button>
                      <button className={`btn ${button.btnPrimary + ' ' + classes.btn}`}>
                      <input type="radio" name="themeselect" />
                        Select
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-6 col-lg-4'>
                <div className={classes.thumblist}>
                  <div className={classes.thumbImg}>
                    <label className={classes.checkitem + ' ' + classes.customChk }>
                      <Image
                        src={Theme6}
                        height={412}
                        width={412}
                        alt="Thumbs"
                      />                  
                      <input type="checkbox" />
                      <span className={classes.checkicon}>                   
                      <FontAwesomeIcon icon={faCheckCircle} className={classes.faCheckCircle} />
                      </span>
                    </label>
                  </div>
                  <div className={classes.thumbInfo}>                 
                    <h3 className={classes.thumbTitle}>
                      <Link href="/"><a>Template Name </a></Link>
                    </h3>
                    <div className={classes.btnGroup}>
                      <button className={`btn ${button.btnOutlinePrimary + ' ' + classes.btn}`} onClick={handleShow}>
                        <FontAwesomeIcon icon={faEye} className={classes.faeye} />
                        Preview
                      </button>
                      <button className={`btn ${button.btnPrimary + ' ' + classes.btn}`}>
                      <input type="radio" name="themeselect" />
                        Select
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Pagination />

            <div className={classes.formGroup + ' ' + classes.btnsGroup + ' ' + classes.textRight }>
              <button className={`btn ${button.btnOutlinePrimary}`}>Cancel</button>
              <button className={`btn ${button.btnPrimary}`}>Save</button>
            </div>
          </div>
          <div className="tab-pane fade" id="category2">
              <h2>Category 2</h2>
          </div>
          <div className="tab-pane fade" id="category3">
              <h2>Category 3</h2>
          </div>
        </div>
      </div>

	<Modal size="lg" show={show} onHide={handleClose}>
		<Modal.Body>
			<h3 className={Modals.head}>Preview of Theme</h3>
			<Slider {...sliderSettings}>
				<div><Image src={ThemeSlider1} alt="Preview Thumb One" layout='responsive' /> </div>
				<div><Image src={ThemeSlider2} alt="Preview Thumb Two" layout='responsive' /> </div>
				<div><Image src={ThemeSlider3} alt="Preview Thumb Three" layout='responsive' /> </div>
				<div><Image src={ThemeSlider4} alt="Preview Thumb Four" layout='responsive' /> </div>
			</Slider>
		</Modal.Body>
	</Modal>
    </PagesAuth>
    </>
  );
}
