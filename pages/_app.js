import React from "react";
import App from 'next/app'
import 'bootstrap/dist/css/bootstrap.css';
import '../public/global.scss';
import '../public/style.scss';

// where the icons flash from a very large icon down to a properly sized one:
import '@fortawesome/fontawesome-svg-core/styles.css';
// Prevent fontawesome from adding its CSS since we did it manually above:
import { config } from '@fortawesome/fontawesome-svg-core';
config.autoAddCss = false; /* eslint-disable import/first */

import "regenerator-runtime/runtime"
import { ThirdwebWeb3Provider } from "@3rdweb/hooks";
const supportedChainIds = [1, 4, 137];
const connectors = {
	injected: {

	}
}

import "regenerator-runtime/runtime";



const MyApp = ({ Component, pageProps, router, user }) => {
	return (
		<ThirdwebWeb3Provider supportedChainIds={supportedChainIds} connectors={connectors}>
			<Component {...pageProps} key={router.route} route={router} user={user} />
		</ThirdwebWeb3Provider>
	)
  }
  MyApp.getInitialProps = async (appContext) => {
	const appProps = await App.getInitialProps(appContext);
	return { ...appProps };
  }
  export default MyApp
  
  